const express = require('express');
// const http = require('http');
const path = require('path');
const loginServer = require('./loginServer');
const login = require('./getEntitlementsServer');
var bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3001;

app.use(express.static(__dirname + '/dist'));
app.use(bodyParser.json({
    limit: '5mb'
}));

app.get('/*', (req, res) => res.sendFile(path.join(__dirname)));

app.use("/api/users", require("./Backend/routes/api/users"));

// const server = http.createServer(app);
//app.post('/api/login',authenticationServer.login);
app.post('/api/login', loginServer.login);

app.listen(port, () => console.log('Running....'));