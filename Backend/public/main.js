(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body>\r\n    <header class=\"navbar dashHeader\">\r\n        <h6 *ngIf=\"loginStatus\">Logged In</h6>\r\n        <h6 *ngIf=\"!loginStatus\">Not Logged In</h6>\r\n\r\n        <div class=\"dropdown show\" style=\"cursor: pointer;margin-right: 50px;\">\r\n        <a class=\"dropdown-toggle\" id=\"dropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"  style=\"float:right; color:white\" ><i class=\"fa fa-fw fa-user\"></i> {{userName$ | async}}</a>\r\n        <div class=\"dropdown-menu\" style=\"min-width: 6rem;\" aria-labelledby=\"dropdownMenuLink\">\r\n          <a class=\"dropdown-item\" style=\"font-size: small;\" (click)=\"logOut()\">Log Out</a>\r\n        </div>\r\n        </div>\r\n      </header>\r\n    <div class=\"container\">\r\n        <div class=\"dashHead\">My Products Dashboard </div>\r\n        <div class=\"container\" style=\"display: flex;\">\r\n          <div class=\"card\" *ngFor=\"let book of books\" style=\"width: 9rem;\">\r\n            <img class=\"img-thumbnail bookImage\" src={{book.bookThumbnail}} alt=\"Card image cap\">\r\n            <div class=\"card-body\">\r\n              <div class=\"card-title\">{{book.bookName}}</div>\r\n              <p class=\"card-text\" style=\"font-size: 10px;\">{{book.bookEdition}}</p>\r\n            </div> \r\n          </div>\r\n          </div>\r\n    </div>\r\n    <div class=\"footer\">\r\n        <a href=\"\" class=\"footerElement\" style=\"border-right: 0em; float: right;\">&copy; Pawandeep Singh</a>\r\n    </div>\r\n    </body>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-page/home-page.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-page/home-page.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"main\">\r\n<header style=\"color:white\">\r\n    \r\n</header>\r\n<div class=\"card pageContent\" >\r\n<div class=\"contentHeading\">Login / Logout Workflow App</div>\r\n<div style=\"text-align: center;font-size: small;\"><p class=\"dlsDescription\"> ION Trading Remote Exercise</p></div>\r\n<button type=\"button\" class=\"loginButton btn btn-light btn-sm\" routerLink=\"/login\">LOGIN</button>\r\n</div>\r\n<div class=\"page-footer footer\" style=\"z-index: 1;\">\r\n    <div>\r\n    <a href=\"\" class=\"footerElement\" style=\"border-right: 0em; float: right;\">&copy; Pawandeep Singh</a>\r\n    </div>\r\n</div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-page/login-page.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-page/login-page.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<body style=\"background-color: none;\">\r\n  <ngx-spinner></ngx-spinner>\r\n\r\n<header style=\"z-index: 1\">\r\n  <h6 *ngIf=\"loginSatus\">Logged In</h6>\r\n  <h6 *ngIf=\"!loginSatus\">Not Logged In</h6>\r\n</header>\r\n<div style=\"display:flex; flex-direction: column; justify-content: space-between;\">\r\n<div class=\"welcomeHead display-4\" style=\"align-self: center;color:white;z-index: 1\">Welcome</div>\r\n<div class=\"card loginCard\">\r\n    <h5 style=\"text-align: center;\" class=\"card-title titleLogin \">Log In</h5>\r\n    <form #loginForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\r\n        <!-- <div class=\"form-group allInputs\">\r\n            <label for=\"exampleInputOrganization\">Organization</label>\r\n            <input type=\"text\" class=\"form-control formInput\" id=\"orgName\" [(ngModel)]=\"model.orgName\" name=\"orgName\" aria-describedby=\"organizationHelp\" placeholder=\"Enter your organization id\">\r\n          </div> -->\r\n        <div class=\"form-group allInputs\">\r\n          <label for=\"exampleInputEmail1\">Username</label>\r\n          <input type=\"email\" class=\"form-control formInput\" id=\"userName\"  [(ngModel)]=\"model.userName\" name=\"userName\" aria-describedby=\"emailHelp\" placeholder=\"Enter your username\">\r\n        </div>\r\n        \r\n        <div class=\"form-group allInputs\">\r\n          <label for=\"exampleInputPassword1\">Password</label>\r\n          <input [type]=\"isActive ? 'password' : 'text'\" class=\"form-control formInput pwdInput\" id=\"passwd\" [(ngModel)]=\"model.passwd\" name=\"passwd\" placeholder=\"Enter your password\">\r\n          <i class=\"fa fa-eye\" style=\"float:right;cursor: pointer;\" (click)=\"toggleView()\"></i>\r\n        </div>\r\n        <div [hidden]=\"isShow\" class=\"invalidCreds\">Your account or password is incorrect. </div>\r\n        <button type=\"submit\" [class.btnDisabled]=\"disableLogin\" [disabled]=\"disableLogin\" class=\"loginPageButton btn btn-light btn-sm\">LOG IN</button>\r\n        \r\n        \r\n      </form>\r\n</div>\r\n</div>\r\n  \r\n\r\n\r\n<div class=\"page-footer footer\">\r\n\r\n    <div>\r\n    <a href=\"\" class=\"footerElement\" style=\"border-right: 0em; float: right;\">&copy; Pawandeep Singh</a>\r\n</div>\r\n  </div>\r\n  \r\n</body>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/app/Samples/dummyUsers.ts":
/*!***************************************!*\
  !*** ./src/app/Samples/dummyUsers.ts ***!
  \***************************************/
/*! exports provided: users */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "users", function() { return users; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var users = [
    {
        username: 'pawan',
        password: 'p1234',
        data: {
            books: [
                {
                    bookName: "Javascript Applications",
                    bookEdition: "Second Edition",
                    bookThumbnail: "assets\\B1.jpg"
                },
                {
                    bookName: "Eloquent Javascript",
                    bookEdition: "Second Edition",
                    bookThumbnail: "assets\\B2.jpg"
                }
            ]
        }
    },
    {
        username: 'test',
        password: 't1234',
        data: {
            books: [
                {
                    bookName: "Javascript Applications",
                    bookEdition: "Second Edition",
                    bookThumbnail: "assets\\B1.jpg"
                },
                {
                    bookName: "Eloquent Javascript",
                    bookEdition: "Second Edition",
                    bookThumbnail: "assets\\B2.jpg"
                }
            ]
        }
    }
];


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login-page/login-page.component */ "./src/app/login-page/login-page.component.ts");
/* harmony import */ var _home_page_home_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home-page/home-page.component */ "./src/app/home-page/home-page.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");






var routes = [
    { path: 'login', component: _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_3__["LoginPageComponent"] },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: _home_page_home_page_component__WEBPACK_IMPORTED_MODULE_4__["HomePageComponent"] },
    { path: 'dashboard/:userName', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app-service.service.ts":
/*!****************************************!*\
  !*** ./src/app/app-service.service.ts ***!
  \****************************************/
/*! exports provided: AppService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppService", function() { return AppService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Samples_dummyUsers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Samples/dummyUsers */ "./src/app/Samples/dummyUsers.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var AppService = /** @class */ (function () {
    function AppService() {
    }
    AppService_1 = AppService;
    AppService.prototype.getUsers = function () {
        console.log(_Samples_dummyUsers__WEBPACK_IMPORTED_MODULE_2__["users"]);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_Samples_dummyUsers__WEBPACK_IMPORTED_MODULE_2__["users"]);
    };
    AppService.prototype.setCurrentUser = function (userName) {
        AppService_1.currentUser = userName;
    };
    AppService.getCurrentUser = function () {
        return AppService_1.currentUser;
    };
    AppService.getCurrentUserBooks = function () {
        console.log(AppService_1.allUsers);
        var userDataNode = AppService_1.allUsers.find(function (user) { return AppService_1.currentUser == user.username; });
        return userDataNode && userDataNode.data.books;
    };
    var AppService_1;
    AppService.allUsers = _Samples_dummyUsers__WEBPACK_IMPORTED_MODULE_2__["users"];
    AppService = AppService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], AppService);
    return AppService;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'DLS-Sample-App';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_page_home_page_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-page/home-page.component */ "./src/app/home-page/home-page.component.ts");
/* harmony import */ var _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login-page/login-page.component */ "./src/app/login-page/login-page.component.ts");
/* harmony import */ var _app_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-service.service */ "./src/app/app-service.service.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.es5.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _home_page_home_page_component__WEBPACK_IMPORTED_MODULE_6__["HomePageComponent"],
                _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_7__["LoginPageComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_9__["DashboardComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_12__["CommonModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                ngx_spinner__WEBPACK_IMPORTED_MODULE_14__["NgxSpinnerModule"],
            ],
            providers: [
                _app_service_service__WEBPACK_IMPORTED_MODULE_8__["AppService"], ngx_cookie_service__WEBPACK_IMPORTED_MODULE_13__["CookieService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.scss":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("body {\n  font-family: Muli,sans-serif;\n  height: -webkit-fill-available; }\n  body:before {\n    content: '';\n    z-index: -1;\n    position: absolute;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    background-size: cover;\n    font-family: Muli,sans-serif; }\n  .card-body {\n  padding: .5rem; }\n  .card {\n  border: none; }\n  h1 {\n  color: navy;\n  margin-left: 20px; }\n  h6 {\n  padding: 10px;\n  color: white;\n  float: right; }\n  .card-title {\n  font-size: 11px;\n  font-weight: 600;\n  margin-bottom: 0; }\n  .bookImage {\n  border: 0em; }\n  .dashHead {\n  font-size: 1.7em;\n  padding: 20px 0px 15px 0px;\n  font-weight: lighter;\n  color: #696969;\n  border-bottom: #696969 1px solid;\n  margin-bottom: 20px;\n  margin-left: 17px; }\n  .footer {\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  padding: 1em;\n  background-color: white;\n  color: slategray;\n  text-align: left;\n  display: inline;\n  font-size: .7em;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n          align-items: center; }\n  .footerElement {\n  border-right: 1px solid #b4b4b4;\n  padding: 2px 4px;\n  margin: 0;\n  color: #696969; }\n  .dashHeader {\n  background-color: black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL0M6XFxQYXdhblxcd2ViYXBwL3NyY1xcYXBwXFxkYXNoYm9hcmRcXGRhc2hib2FyZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLDRCQUE0QjtFQUM1Qiw4QkFBOEIsRUFBQTtFQUYvQjtJQUlFLFdBQVc7SUFDWCxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxRQUFRO0lBQ1IsTUFBTTtJQUNOLFNBQVM7SUFDUixzQkFBc0I7SUFFdkIsNEJBQTRCLEVBQUE7RUFHOUI7RUFDQyxjQUFjLEVBQUE7RUFFZjtFQUNDLFlBQVksRUFBQTtFQUViO0VBQ0MsV0FBVztFQUNYLGlCQUFpQixFQUFBO0VBRWxCO0VBQ0MsYUFBWTtFQUNaLFlBQVc7RUFDWCxZQUNBLEVBQUE7RUFDRDtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7RUFFakI7RUFDQyxXQUFXLEVBQUE7RUFFWjtFQUNDLGdCQUFnQjtFQUNoQiwwQkFBMEI7RUFDMUIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxnQ0FBZ0M7RUFDaEMsbUJBQW1CO0VBQ25CLGlCQUFpQixFQUFBO0VBRWxCO0VBQ0MsZUFBZTtFQUNmLE9BQU87RUFDUCxTQUFTO0VBQ1QsWUFBWTtFQUNaLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixlQUFlO0VBQ2Ysb0JBQWE7RUFBYixhQUFhO0VBQ2IseUJBQThCO1VBQTlCLDhCQUE4QjtFQUM5Qix5QkFBbUI7VUFBbkIsbUJBQW1CLEVBQUE7RUFFcEI7RUFDQywrQkFBK0I7RUFDL0IsZ0JBQWdCO0VBQ2hCLFNBQVM7RUFDVCxjQUFjLEVBQUE7RUFFZjtFQUNDLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHkge1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0aGVpZ2h0OiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xyXG5cdCY6YmVmb3JlIHtcclxuXHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0ei1pbmRleDogLTE7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRsZWZ0OiAwO1xyXG5cdFx0cmlnaHQ6IDA7XHJcblx0XHR0b3A6IDA7XHJcblx0XHRib3R0b206IDA7XHJcblx0XHRcdGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcblx0XHQvL2Rpc3BsYXk6IGZsZXg7XHJcblx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdH1cclxufVxyXG4uY2FyZC1ib2R5IHtcclxuXHRwYWRkaW5nOiAuNXJlbTtcclxufVxyXG4uY2FyZCB7XHJcblx0Ym9yZGVyOiBub25lO1xyXG59XHJcbmgxIHtcclxuXHRjb2xvcjogbmF2eTtcclxuXHRtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5oNntcclxuXHRwYWRkaW5nOjEwcHg7XHJcblx0Y29sb3I6d2hpdGU7XHJcblx0ZmxvYXQ6cmlnaHRcclxuXHR9XHJcbi5jYXJkLXRpdGxlIHtcclxuXHRmb250LXNpemU6IDExcHg7XHJcblx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcbi5ib29rSW1hZ2Uge1xyXG5cdGJvcmRlcjogMGVtO1xyXG59XHJcbi5kYXNoSGVhZCB7XHJcblx0Zm9udC1zaXplOiAxLjdlbTtcclxuXHRwYWRkaW5nOiAyMHB4IDBweCAxNXB4IDBweDtcclxuXHRmb250LXdlaWdodDogbGlnaHRlcjtcclxuXHRjb2xvcjogIzY5Njk2OTtcclxuXHRib3JkZXItYm90dG9tOiAjNjk2OTY5IDFweCBzb2xpZDtcclxuXHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cdG1hcmdpbi1sZWZ0OiAxN3B4O1xyXG59XHJcbi5mb290ZXIge1xyXG5cdHBvc2l0aW9uOiBmaXhlZDtcclxuXHRsZWZ0OiAwO1xyXG5cdGJvdHRvbTogMDtcclxuXHRwYWRkaW5nOiAxZW07XHJcblx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcblx0Y29sb3I6IHNsYXRlZ3JheTtcclxuXHR0ZXh0LWFsaWduOiBsZWZ0O1xyXG5cdGRpc3BsYXk6IGlubGluZTtcclxuXHRmb250LXNpemU6IC43ZW07XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uZm9vdGVyRWxlbWVudCB7XHJcblx0Ym9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2I0YjRiNDtcclxuXHRwYWRkaW5nOiAycHggNHB4O1xyXG5cdG1hcmdpbjogMDtcclxuXHRjb2xvcjogIzY5Njk2OTtcclxufVxyXG4uZGFzaEhlYWRlciB7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _Samples_dummyUsers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Samples/dummyUsers */ "./src/app/Samples/dummyUsers.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/ngx-cookie-service.es5.js");







var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(route, router, loginService, ref, cookieService) {
        this.route = route;
        this.router = router;
        this.loginService = loginService;
        this.ref = ref;
        this.cookieService = cookieService;
        this.currentUser = "";
        this.loginStatus = false;
        this.allUsers = _Samples_dummyUsers__WEBPACK_IMPORTED_MODULE_4__["users"];
    }
    DashboardComponent.prototype.getCurrentUser = function () {
        var cookieValue = this.cookieService.get('data');
        this.currentUser = cookieValue.split('-')[0];
    };
    DashboardComponent.prototype.logOut = function () {
        this.cookieService.delete('data', '/', window.location.hostname);
        this.router.navigate(['/login']);
    };
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loginService.isAuthenticated().subscribe(function (data) {
            if (data['isAuthenticated']) {
                _this.loginStatus = true;
                _this.ref.tick();
                _this.getCurrentUser();
                _this.books = _this.allUsers.find(function (user) {
                    if (user.username.toLowerCase() == _this.currentUser.toLowerCase()) {
                        return true;
                    }
                }).data.books;
            }
            else {
                _this.router.navigate(['/login']);
            }
        }, function (err) {
            if (err.status == 401) {
                _this.router.navigate(['/login']);
            }
        });
    };
    DashboardComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
        { type: _login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"] },
        { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieService"] }
    ]; };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/dashboard/dashboard.component.scss")).default]
        })
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/home-page/home-page.component.scss":
/*!****************************************************!*\
  !*** ./src/app/home-page/home-page.component.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".main {\n  font-family: Muli,sans-serif; }\n  .main::before {\n    content: '';\n    z-index: 0;\n    position: absolute;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    -webkit-filter: blur(2px);\n    filter: brightness(40%);\n    background: url('macbook wallpaper.jpg') no-repeat;\n    background-size: cover;\n    font-family: Muli,sans-serif;\n    z-index: -1; }\n  .main::before .main {\n      font-family: Muli,sans-serif; }\n  .main::before .main::before {\n        content: '';\n        z-index: 0;\n        position: absolute;\n        left: 0;\n        right: 0;\n        top: 0;\n        bottom: 0;\n        -webkit-filter: blur(2px);\n        filter: brightness(40%);\n        background: url('macbook wallpaper.jpg') no-repeat;\n        background-size: cover;\n        font-family: Muli,sans-serif;\n        z-index: -1; }\n  .main::before .contentHeading {\n      text-align: center;\n      color: white;\n      font-size: 2.5em;\n      padding-bottom: 15px; }\n  .main::before h1 {\n      color: navy;\n      margin-left: 20px; }\n  .main::before .footer {\n      position: fixed;\n      left: 0;\n      bottom: 0;\n      width: 100%;\n      padding: 1em;\n      background-color: white;\n      color: slategray;\n      text-align: left;\n      display: inline;\n      font-size: .7em;\n      display: -webkit-box;\n      display: flex;\n      -webkit-box-pack: justify;\n              justify-content: space-between;\n      -webkit-box-align: center;\n              align-items: center; }\n  .main::before .footerElement {\n      border-right: 1px solid #b4b4b4;\n      padding: 2px 4px;\n      margin: 0;\n      color: #696969; }\n  .main::before .background {\n      -webkit-filter: blur(5px);\n              filter: blur(5px);\n      -webkit-filter: brightness(100%);\n              filter: brightness(100%); }\n  .main::before .pageContent {\n      padding-top: 100px;\n      height: inherit;\n      display: -webkit-box;\n      display: flex;\n      background: none;\n      border: none;\n      text-align: center;\n      color: white;\n      font-family: Muli,sans-serif;\n      -webkit-box-align: center;\n              align-items: center;\n      -webkit-box-pack: start;\n              justify-content: flex-start;\n      align-self: center;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n              flex-direction: column; }\n  .main::before .loginButton {\n      background-color: white;\n      color: #8723ff;\n      border-radius: 2em;\n      border-color: white;\n      font-family: Muli,sans-serif;\n      font-size: .7em;\n      font-weight: bolder;\n      padding: 7px 12px 7px 12px; }\n  .main::before .loginButton:hover {\n        background-color: white;\n        color: #8723ff;\n        border-radius: 2em;\n        border-color: white;\n        font-family: Muli,sans-serif;\n        font-size: .7em;\n        font-weight: bolder;\n        padding: 7px 12px 7px 12px; }\n  .main::before .loginPageButton {\n      background-color: #8723ff;\n      color: white;\n      border-radius: 2em;\n      border-color: #8723ff;\n      font-family: Muli,sans-serif;\n      font-size: .7em;\n      font-weight: 900;\n      padding: 10px 20px 10px 20px;\n      margin-top: 13px;\n      margin-bottom: 25px;\n      width: 87px;\n      position: absolute;\n      left: 25%; }\n  .main::before .loginPageButton:hover {\n        background-color: #8723ff;\n        color: white;\n        border-radius: 2em;\n        border-color: #8723ff;\n        font-family: Muli,sans-serif;\n        font-size: .7em;\n        font-weight: 900;\n        padding: 10px 20px 10px 20px;\n        margin-top: 13px;\n        margin-bottom: 25px;\n        width: 87px;\n        position: absolute;\n        left: 25%; }\n  .main::before .loginCard.card {\n      display: -webkit-box;\n      display: flex;\n      width: 325px;\n      min-height: 370px;\n      font-family: Muli,sans-serif;\n      position: absolute;\n      top: 40%;\n      left: 50%;\n      margin-right: -50%;\n      -webkit-transform: translate(-50%, -50%);\n              transform: translate(-50%, -50%);\n      -webkit-box-align: center;\n              align-items: center;\n      -webkit-box-pack: center;\n              justify-content: center; }\n  .main::before label {\n      margin-bottom: 0em;\n      font-size: .6em;\n      color: slategray;\n      font-weight: 800; }\n  .main::before .titleLogin {\n      text-align: center;\n      margin-bottom: 45px;\n      margin-top: 15px;\n      font-size: 17px;\n      font-weight: 800; }\n  .main::before .formInput {\n      font-size: 12px;\n      font-family: Muli,sans-serif;\n      background-color: #F5F5F5;\n      border-color: #F5F5F5; }\n  .main::before .allInputs {\n      padding: 0px 25px 0px 25px; }\n  .main::before .pwdInput {\n      width: 273px;\n      display: inline-block; }\n  .main::before .fa {\n      position: absolute;\n      top: 255px;\n      right: 35px;\n      color: #8723ff; }\n  .main .contentHeading {\n    text-align: center;\n    color: white;\n    font-size: 2.5em;\n    padding-bottom: 15px; }\n  .main h1 {\n    color: navy;\n    margin-left: 20px; }\n  .main .footer {\n    position: fixed;\n    left: 0;\n    bottom: 0;\n    width: 100%;\n    padding: 1em;\n    background-color: white;\n    color: slategray;\n    text-align: left;\n    display: inline;\n    font-size: .7em;\n    display: -webkit-box;\n    display: flex;\n    -webkit-box-pack: justify;\n            justify-content: space-between;\n    -webkit-box-align: center;\n            align-items: center; }\n  .main .footerElement {\n    border-right: 1px solid #b4b4b4;\n    padding: 2px 4px;\n    margin: 0;\n    color: #696969; }\n  .main .background {\n    -webkit-filter: blur(5px);\n            filter: blur(5px);\n    -webkit-filter: brightness(100%);\n            filter: brightness(100%); }\n  .main .dlsDescription {\n    text-align: center;\n    padding: 10px;\n    min-width: 290px; }\n  @media (min-width: 576px) {\n      .main .dlsDescription {\n        width: 325px; } }\n  .main .pageContent {\n    padding-top: 100px;\n    height: inherit;\n    display: -webkit-box;\n    display: flex;\n    background: none;\n    border: none;\n    text-align: center;\n    color: white;\n    font-family: Muli,sans-serif;\n    -webkit-box-align: center;\n            align-items: center;\n    -webkit-box-pack: start;\n            justify-content: flex-start;\n    align-self: center;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n            flex-direction: column; }\n  .main .loginButton {\n    background-color: white;\n    color: #8723ff;\n    border-radius: 2em;\n    border-color: white;\n    font-family: Muli,sans-serif;\n    font-size: .7em;\n    font-weight: bolder;\n    padding: 7px 12px 7px 12px; }\n  .main .loginButton:hover {\n      background-color: white;\n      color: #8723ff;\n      border-radius: 2em;\n      border-color: white;\n      font-family: Muli,sans-serif;\n      font-size: .7em;\n      font-weight: bolder;\n      padding: 7px 12px 7px 12px; }\n  .main .loginPageButton {\n    background-color: #8723ff;\n    color: white;\n    border-radius: 2em;\n    border-color: #8723ff;\n    font-family: Muli,sans-serif;\n    font-size: .7em;\n    font-weight: 900;\n    padding: 10px 20px 10px 20px;\n    margin-top: 13px;\n    margin-bottom: 25px;\n    width: 87px;\n    position: absolute;\n    left: 25%; }\n  .main .loginPageButton:hover {\n      background-color: #8723ff;\n      color: white;\n      border-radius: 2em;\n      border-color: #8723ff;\n      font-family: Muli,sans-serif;\n      font-size: .7em;\n      font-weight: 900;\n      padding: 10px 20px 10px 20px;\n      margin-top: 13px;\n      margin-bottom: 25px;\n      width: 87px;\n      position: absolute;\n      left: 25%; }\n  .main .loginCard.card {\n    display: -webkit-box;\n    display: flex;\n    width: 325px;\n    min-height: 370px;\n    font-family: Muli,sans-serif;\n    position: absolute;\n    top: 40%;\n    left: 50%;\n    margin-right: -50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%);\n    -webkit-box-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n            justify-content: center; }\n  .main label {\n    margin-bottom: 0em;\n    font-size: .6em;\n    color: slategray;\n    font-weight: 800; }\n  .main .titleLogin {\n    text-align: center;\n    margin-bottom: 45px;\n    margin-top: 15px;\n    font-size: 17px;\n    font-weight: 800; }\n  .main .formInput {\n    font-size: 12px;\n    font-family: Muli,sans-serif;\n    background-color: #F5F5F5;\n    border-color: #F5F5F5; }\n  .main .allInputs {\n    padding: 0px 25px 0px 25px; }\n  .main .pwdInput {\n    width: 273px;\n    display: inline-block; }\n  .main .fa {\n    position: absolute;\n    top: 255px;\n    right: 35px;\n    color: #8723ff; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS1wYWdlL0M6XFxQYXdhblxcd2ViYXBwL3NyY1xcYXBwXFxob21lLXBhZ2VcXGhvbWUtcGFnZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvaG9tZS1wYWdlL0M6XFxQYXdhblxcd2ViYXBwL25vZGVfbW9kdWxlc1xcYm9vdHN0cmFwXFxzY3NzXFxtaXhpbnNcXF9icmVha3BvaW50cy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0MsNEJBQTRCLEVBQUE7RUFEN0I7SUFHRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AsUUFBUTtJQUNSLE1BQU07SUFDTixTQUFTO0lBQ1QseUJBQXlCO0lBQ3pCLHVCQUF1QjtJQUN2QixrREFBK0Q7SUFDL0Qsc0JBQXNCO0lBRXRCLDRCQUE0QjtJQUM1QixXQUFXLEVBQUE7RUFoQmI7TUFpQkcsNEJBQTRCLEVBQUE7RUFqQi9CO1FBb0JLLFdBQVc7UUFDWCxVQUFVO1FBQ1Ysa0JBQWtCO1FBQ2xCLE9BQU87UUFDUCxRQUFRO1FBQ1IsTUFBTTtRQUNOLFNBQVM7UUFDVCx5QkFBeUI7UUFDekIsdUJBQXVCO1FBQ3ZCLGtEQUErRDtRQUMvRCxzQkFBc0I7UUFFdEIsNEJBQTRCO1FBQzVCLFdBQVcsRUFBQTtFQWpDaEI7TUFzQ0csa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixnQkFBZ0I7TUFDaEIsb0JBQW9CLEVBQUE7RUF6Q3ZCO01BNkNHLFdBQVc7TUFDWCxpQkFBaUIsRUFBQTtFQTlDcEI7TUFrREcsZUFBZTtNQUNmLE9BQU87TUFDUCxTQUFTO01BQ1QsV0FBVztNQUNYLFlBQVk7TUFDWix1QkFBdUI7TUFDdkIsZ0JBQWdCO01BQ2hCLGdCQUFnQjtNQUNoQixlQUFlO01BQ2YsZUFBZTtNQUNmLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUE4QjtjQUE5Qiw4QkFBOEI7TUFDOUIseUJBQW1CO2NBQW5CLG1CQUFtQixFQUFBO0VBOUR0QjtNQWtFRywrQkFBK0I7TUFDL0IsZ0JBQWdCO01BQ2hCLFNBQVM7TUFDVCxjQUFjLEVBQUE7RUFyRWpCO01BeUVHLHlCQUFpQjtjQUFqQixpQkFBaUI7TUFDakIsZ0NBQXdCO2NBQXhCLHdCQUF3QixFQUFBO0VBMUUzQjtNQTZFRyxrQkFBa0I7TUFDbEIsZUFBZTtNQUNmLG9CQUFhO01BQWIsYUFBYTtNQUNiLGdCQUFnQjtNQUNoQixZQUFZO01BQ1osa0JBQWtCO01BQ2xCLFlBQVk7TUFDWiw0QkFBNEI7TUFDNUIseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQix1QkFBMkI7Y0FBM0IsMkJBQTJCO01BQzNCLGtCQUFrQjtNQUNsQiw0QkFBc0I7TUFBdEIsNkJBQXNCO2NBQXRCLHNCQUFzQixFQUFBO0VBeEZ6QjtNQTRGRyx1QkFBdUI7TUFDdkIsY0FBYztNQUNkLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsNEJBQTRCO01BQzVCLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsMEJBQTBCLEVBQUE7RUFuRzdCO1FBc0dLLHVCQUF1QjtRQUN2QixjQUFjO1FBQ2Qsa0JBQWtCO1FBQ2xCLG1CQUFtQjtRQUNuQiw0QkFBNEI7UUFDNUIsZUFBZTtRQUNmLG1CQUFtQjtRQUNuQiwwQkFBMEIsRUFBQTtFQTdHL0I7TUFrSEcseUJBQXlCO01BQ3pCLFlBQVk7TUFDWixrQkFBa0I7TUFDbEIscUJBQXFCO01BQ3JCLDRCQUE0QjtNQUM1QixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLDRCQUE0QjtNQUM1QixnQkFBZ0I7TUFDaEIsbUJBQW1CO01BQ25CLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIsU0FBUyxFQUFBO0VBOUhaO1FBaUlLLHlCQUF5QjtRQUN6QixZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLHFCQUFxQjtRQUNyQiw0QkFBNEI7UUFDNUIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQiw0QkFBNEI7UUFDNUIsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLFNBQVMsRUFBQTtFQTdJZDtNQWtKRyxvQkFBYTtNQUFiLGFBQWE7TUFDYixZQUFZO01BQ1osaUJBQWlCO01BQ2pCLDRCQUE0QjtNQUM1QixrQkFBa0I7TUFDbEIsUUFBUTtNQUNSLFNBQVM7TUFDVCxrQkFBa0I7TUFDbEIsd0NBQWdDO2NBQWhDLGdDQUFnQztNQUNoQyx5QkFBbUI7Y0FBbkIsbUJBQW1CO01BQ25CLHdCQUF1QjtjQUF2Qix1QkFBdUIsRUFBQTtFQTVKMUI7TUFnS0csa0JBQWtCO01BQ2xCLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsZ0JBQWdCLEVBQUE7RUFuS25CO01BdUtHLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsZ0JBQWdCO01BQ2hCLGVBQWU7TUFDZixnQkFBZ0IsRUFBQTtFQTNLbkI7TUErS0csZUFBZTtNQUNmLDRCQUE0QjtNQUM1Qix5QkFBeUI7TUFDekIscUJBQXFCLEVBQUE7RUFsTHhCO01Bc0xHLDBCQUEwQixFQUFBO0VBdEw3QjtNQTBMRyxZQUFZO01BQ1oscUJBQXFCLEVBQUE7RUEzTHhCO01BK0xHLGtCQUFrQjtNQUNsQixVQUFVO01BQ1YsV0FBVztNQUNYLGNBQWMsRUFBQTtFQWxNakI7SUF1TUMsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsb0JBQW9CLEVBQUE7RUExTXJCO0lBNk1DLFdBQVc7SUFDWCxpQkFBaUIsRUFBQTtFQTlNbEI7SUFpTkMsZUFBZTtJQUNmLE9BQU87SUFDUCxTQUFTO0lBQ1QsV0FBVztJQUNYLFlBQVk7SUFDWix1QkFBdUI7SUFDdkIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZUFBZTtJQUNmLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHlCQUE4QjtZQUE5Qiw4QkFBOEI7SUFDOUIseUJBQW1CO1lBQW5CLG1CQUFtQixFQUFBO0VBN05wQjtJQWdPQywrQkFBK0I7SUFDL0IsZ0JBQWdCO0lBQ2hCLFNBQVM7SUFDVCxjQUFjLEVBQUE7RUFuT2Y7SUFzT0MseUJBQWlCO1lBQWpCLGlCQUFpQjtJQUNqQixnQ0FBd0I7WUFBeEIsd0JBQXdCLEVBQUE7RUF2T3pCO0lBME9DLGtCQUFrQjtJQUNsQixhQUFhO0lBQ1osZ0JBQWdCLEVBQUE7RUNwTGQ7TUR4REo7UUE4T0ksWUFBWSxFQUFBLEVBRWQ7RUFoUEY7SUFtUEUsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixvQkFBYTtJQUFiLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osNEJBQTRCO0lBQzVCLHlCQUFtQjtZQUFuQixtQkFBbUI7SUFDbkIsdUJBQTJCO1lBQTNCLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIsNEJBQXNCO0lBQXRCLDZCQUFzQjtZQUF0QixzQkFBc0IsRUFBQTtFQTlQeEI7SUFrUUMsdUJBQXVCO0lBQ3ZCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLDRCQUE0QjtJQUM1QixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLDBCQUEwQixFQUFBO0VBelEzQjtNQTJRRSx1QkFBdUI7TUFDdkIsY0FBYztNQUNkLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsNEJBQTRCO01BQzVCLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsMEJBQTBCLEVBQUE7RUFsUjVCO0lBc1JDLHlCQUF5QjtJQUN6QixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQiw0QkFBNEI7SUFDNUIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQiw0QkFBNEI7SUFDNUIsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFNBQVMsRUFBQTtFQWxTVjtNQW9TRSx5QkFBeUI7TUFDekIsWUFBWTtNQUNaLGtCQUFrQjtNQUNsQixxQkFBcUI7TUFDckIsNEJBQTRCO01BQzVCLGVBQWU7TUFDZixnQkFBZ0I7TUFDaEIsNEJBQTRCO01BQzVCLGdCQUFnQjtNQUNoQixtQkFBbUI7TUFDbkIsV0FBVztNQUNYLGtCQUFrQjtNQUNsQixTQUFTLEVBQUE7RUFoVFg7SUFvVEMsb0JBQWE7SUFBYixhQUFhO0lBQ2IsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQiw0QkFBNEI7SUFDNUIsa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1Qsa0JBQWtCO0lBQ2xCLHdDQUFnQztZQUFoQyxnQ0FBZ0M7SUFDaEMseUJBQW1CO1lBQW5CLG1CQUFtQjtJQUNuQix3QkFBdUI7WUFBdkIsdUJBQXVCLEVBQUE7RUE5VHhCO0lBaVVDLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGdCQUFnQixFQUFBO0VBcFVqQjtJQXVVQyxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGdCQUFnQjtJQUNoQixlQUFlO0lBQ2YsZ0JBQWdCLEVBQUE7RUEzVWpCO0lBOFVDLGVBQWU7SUFDZiw0QkFBNEI7SUFDNUIseUJBQXlCO0lBQ3pCLHFCQUFxQixFQUFBO0VBalZ0QjtJQW9WQywwQkFBMEIsRUFBQTtFQXBWM0I7SUF1VkMsWUFBWTtJQUNaLHFCQUFxQixFQUFBO0VBeFZ0QjtJQTJWQyxrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFdBQVc7SUFDWCxjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ob21lLXBhZ2UvaG9tZS1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9fZnVuY3Rpb25zLnNjc3NcIjtcclxuQGltcG9ydCBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9fdmFyaWFibGVzLnNjc3NcIjtcclxuQGltcG9ydCBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9taXhpbnMvX2JyZWFrcG9pbnRzLnNjc3NcIjtcclxuXHJcbi5tYWluIHtcclxuXHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdCY6OmJlZm9yZSB7XHJcblx0XHRjb250ZW50OiAnJztcclxuXHRcdHotaW5kZXg6IDA7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRsZWZ0OiAwO1xyXG5cdFx0cmlnaHQ6IDA7XHJcblx0XHR0b3A6IDA7XHJcblx0XHRib3R0b206IDA7XHJcblx0XHQtd2Via2l0LWZpbHRlcjogYmx1cigycHgpO1xyXG5cdFx0ZmlsdGVyOiBicmlnaHRuZXNzKDQwJSk7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoJ3NyY1xcXFxhc3NldHNcXFxcbWFjYm9vayB3YWxscGFwZXIuanBnJykgbm8tcmVwZWF0O1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHRcdC8vZGlzcGxheTogZmxleDtcclxuXHRcdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0XHR6LWluZGV4OiAtMTsubWFpbiB7XHJcblx0XHRcdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0XHQgIFxyXG5cdFx0XHQmOjpiZWZvcmUge1xyXG5cdFx0XHQgIGNvbnRlbnQ6ICcnO1xyXG5cdFx0XHQgIHotaW5kZXg6IDA7XHJcblx0XHRcdCAgcG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHQgIGxlZnQ6IDA7XHJcblx0XHRcdCAgcmlnaHQ6IDA7XHJcblx0XHRcdCAgdG9wOiAwO1xyXG5cdFx0XHQgIGJvdHRvbTogMDtcclxuXHRcdFx0ICAtd2Via2l0LWZpbHRlcjogYmx1cigycHgpO1xyXG5cdFx0XHQgIGZpbHRlcjogYnJpZ2h0bmVzcyg0MCUpO1xyXG5cdFx0XHQgIGJhY2tncm91bmQ6IHVybCgnc3JjXFxcXGFzc2V0c1xcXFxtYWNib29rIHdhbGxwYXBlci5qcGcnKSBuby1yZXBlYXQ7XHJcblx0XHRcdCAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHRcdFx0ICAvL2Rpc3BsYXk6IGZsZXg7XHJcblx0XHRcdCAgZm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcclxuXHRcdFx0ICB6LWluZGV4OiAtMTtcclxuXHRcdFx0fVxyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAuY29udGVudEhlYWRpbmcge1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0Zm9udC1zaXplOiAyLjVlbTtcclxuXHRcdFx0cGFkZGluZy1ib3R0b206IDE1cHg7XHJcblx0XHQgIH1cclxuXHRcdCAgXHJcblx0XHQgIGgxIHtcclxuXHRcdFx0Y29sb3I6IG5hdnk7XHJcblx0XHRcdG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAuZm9vdGVyIHtcclxuXHRcdFx0cG9zaXRpb246IGZpeGVkO1xyXG5cdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHRib3R0b206IDA7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRwYWRkaW5nOiAxZW07XHJcblx0XHRcdGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5cdFx0XHRjb2xvcjogc2xhdGVncmF5O1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBsZWZ0O1xyXG5cdFx0XHRkaXNwbGF5OiBpbmxpbmU7XHJcblx0XHRcdGZvbnQtc2l6ZTogLjdlbTtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0anVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cdFx0XHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAuZm9vdGVyRWxlbWVudCB7XHJcblx0XHRcdGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNiNGI0YjQ7XHJcblx0XHRcdHBhZGRpbmc6IDJweCA0cHg7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0Y29sb3I6ICM2OTY5Njk7XHJcblx0XHQgIH1cclxuXHRcdCAgXHJcblx0XHQgIC5iYWNrZ3JvdW5kIHtcclxuXHRcdFx0ZmlsdGVyOiBibHVyKDVweCk7XHJcblx0XHRcdGZpbHRlcjogYnJpZ2h0bmVzcygxMDAlKTtcclxuXHRcdCAgfVxyXG5cdFx0ICAucGFnZUNvbnRlbnQge1xyXG5cdFx0XHRwYWRkaW5nLXRvcDogMTAwcHg7XHJcblx0XHRcdGhlaWdodDogaW5oZXJpdDtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0YmFja2dyb3VuZDogbm9uZTtcclxuXHRcdFx0Ym9yZGVyOiBub25lO1xyXG5cdFx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHRcdGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0Zm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcclxuXHRcdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdFx0anVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG5cdFx0XHRhbGlnbi1zZWxmOiBjZW50ZXI7XHJcblx0XHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0XHQgIH1cclxuXHRcdCAgXHJcblx0XHQgIC5sb2dpbkJ1dHRvbiB7XHJcblx0XHRcdGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5cdFx0XHRjb2xvcjogIzg3MjNmZjtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogMmVtO1xyXG5cdFx0XHRib3JkZXItY29sb3I6IHdoaXRlO1xyXG5cdFx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0XHRmb250LXNpemU6IC43ZW07XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcblx0XHRcdHBhZGRpbmc6IDdweCAxMnB4IDdweCAxMnB4O1xyXG5cdFx0ICBcclxuXHRcdFx0Jjpob3ZlciB7XHJcblx0XHRcdCAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcblx0XHRcdCAgY29sb3I6ICM4NzIzZmY7XHJcblx0XHRcdCAgYm9yZGVyLXJhZGl1czogMmVtO1xyXG5cdFx0XHQgIGJvcmRlci1jb2xvcjogd2hpdGU7XHJcblx0XHRcdCAgZm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcclxuXHRcdFx0ICBmb250LXNpemU6IC43ZW07XHJcblx0XHRcdCAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuXHRcdFx0ICBwYWRkaW5nOiA3cHggMTJweCA3cHggMTJweDtcclxuXHRcdFx0fVxyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAubG9naW5QYWdlQnV0dG9uIHtcclxuXHRcdFx0YmFja2dyb3VuZC1jb2xvcjogIzg3MjNmZjtcclxuXHRcdFx0Y29sb3I6IHdoaXRlO1xyXG5cdFx0XHRib3JkZXItcmFkaXVzOiAyZW07XHJcblx0XHRcdGJvcmRlci1jb2xvcjogIzg3MjNmZjtcclxuXHRcdFx0Zm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcclxuXHRcdFx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdFx0XHRmb250LXdlaWdodDogOTAwO1xyXG5cdFx0XHRwYWRkaW5nOiAxMHB4IDIwcHggMTBweCAyMHB4O1xyXG5cdFx0XHRtYXJnaW4tdG9wOiAxM3B4O1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cdFx0XHR3aWR0aDogODdweDtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRsZWZ0OiAyNSU7XHJcblx0XHQgIFxyXG5cdFx0XHQmOmhvdmVyIHtcclxuXHRcdFx0ICBiYWNrZ3JvdW5kLWNvbG9yOiAjODcyM2ZmO1xyXG5cdFx0XHQgIGNvbG9yOiB3aGl0ZTtcclxuXHRcdFx0ICBib3JkZXItcmFkaXVzOiAyZW07XHJcblx0XHRcdCAgYm9yZGVyLWNvbG9yOiAjODcyM2ZmO1xyXG5cdFx0XHQgIGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0XHRcdCAgZm9udC1zaXplOiAuN2VtO1xyXG5cdFx0XHQgIGZvbnQtd2VpZ2h0OiA5MDA7XHJcblx0XHRcdCAgcGFkZGluZzogMTBweCAyMHB4IDEwcHggMjBweDtcclxuXHRcdFx0ICBtYXJnaW4tdG9wOiAxM3B4O1xyXG5cdFx0XHQgIG1hcmdpbi1ib3R0b206IDI1cHg7XHJcblx0XHRcdCAgd2lkdGg6IDg3cHg7XHJcblx0XHRcdCAgcG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHQgIGxlZnQ6IDI1JTtcclxuXHRcdFx0fVxyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAubG9naW5DYXJkLmNhcmQge1xyXG5cdFx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0XHR3aWR0aDogMzI1cHg7XHJcblx0XHRcdG1pbi1oZWlnaHQ6IDM3MHB4O1xyXG5cdFx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdHRvcDogNDAlO1xyXG5cdFx0XHRsZWZ0OiA1MCU7XHJcblx0XHRcdG1hcmdpbi1yaWdodDogLTUwJTtcclxuXHRcdFx0dHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcblx0XHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICBsYWJlbCB7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDBlbTtcclxuXHRcdFx0Zm9udC1zaXplOiAuNmVtO1xyXG5cdFx0XHRjb2xvcjogc2xhdGVncmF5O1xyXG5cdFx0XHRmb250LXdlaWdodDogODAwO1xyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAudGl0bGVMb2dpbiB7XHJcblx0XHRcdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRcdFx0bWFyZ2luLWJvdHRvbTogNDVweDtcclxuXHRcdFx0bWFyZ2luLXRvcDogMTVweDtcclxuXHRcdFx0Zm9udC1zaXplOiAxN3B4O1xyXG5cdFx0XHRmb250LXdlaWdodDogODAwO1xyXG5cdFx0ICB9XHJcblx0XHQgIFxyXG5cdFx0ICAuZm9ybUlucHV0IHtcclxuXHRcdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0XHRiYWNrZ3JvdW5kLWNvbG9yOiAjRjVGNUY1O1xyXG5cdFx0XHRib3JkZXItY29sb3I6ICNGNUY1RjU7XHJcblx0XHQgIH1cclxuXHRcdCAgXHJcblx0XHQgIC5hbGxJbnB1dHMge1xyXG5cdFx0XHRwYWRkaW5nOiAwcHggMjVweCAwcHggMjVweDtcclxuXHRcdCAgfVxyXG5cdFx0ICBcclxuXHRcdCAgLnB3ZElucHV0IHtcclxuXHRcdFx0d2lkdGg6IDI3M3B4O1xyXG5cdFx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHQgIH1cclxuXHRcdCAgXHJcblx0XHQgIC5mYSB7XHJcblx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdFx0dG9wOiAyNTVweDtcclxuXHRcdFx0cmlnaHQ6IDM1cHg7XHJcblx0XHRcdGNvbG9yOiAjODcyM2ZmO1xyXG5cdFx0ICB9XHJcblx0fVxyXG5cclxuLmNvbnRlbnRIZWFkaW5nIHtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0Y29sb3I6IHdoaXRlO1xyXG5cdGZvbnQtc2l6ZTogMi41ZW07XHJcblx0cGFkZGluZy1ib3R0b206IDE1cHg7XHJcbn1cclxuaDEge1xyXG5cdGNvbG9yOiBuYXZ5O1xyXG5cdG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcbi5mb290ZXIge1xyXG5cdHBvc2l0aW9uOiBmaXhlZDtcclxuXHRsZWZ0OiAwO1xyXG5cdGJvdHRvbTogMDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRwYWRkaW5nOiAxZW07XHJcblx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcblx0Y29sb3I6IHNsYXRlZ3JheTtcclxuXHR0ZXh0LWFsaWduOiBsZWZ0O1xyXG5cdGRpc3BsYXk6IGlubGluZTtcclxuXHRmb250LXNpemU6IC43ZW07XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uZm9vdGVyRWxlbWVudCB7XHJcblx0Ym9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2I0YjRiNDtcclxuXHRwYWRkaW5nOiAycHggNHB4O1xyXG5cdG1hcmdpbjogMDtcclxuXHRjb2xvcjogIzY5Njk2OTtcclxufVxyXG4uYmFja2dyb3VuZCB7XHJcblx0ZmlsdGVyOiBibHVyKDVweCk7XHJcblx0ZmlsdGVyOiBicmlnaHRuZXNzKDEwMCUpO1xyXG59XHJcbiAuZGxzRGVzY3JpcHRpb24ge1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRwYWRkaW5nOiAxMHB4O1xyXG4gIG1pbi13aWR0aDogMjkwcHg7XHJcblx0QGluY2x1ZGUgbWVkaWEtYnJlYWtwb2ludC11cChzbSkgeyBcclxuICAgIHdpZHRoOiAzMjVweDtcclxufVxyXG4gfVxyXG5cclxuXHQucGFnZUNvbnRlbnQge1xyXG5cdFx0cGFkZGluZy10b3A6IDEwMHB4O1xyXG5cdFx0aGVpZ2h0OiBpbmhlcml0O1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdGJhY2tncm91bmQ6IG5vbmU7XHJcblx0XHRib3JkZXI6IG5vbmU7XHJcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuXHRcdGFsaWduLXNlbGY6IGNlbnRlcjtcclxuXHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0fVxyXG5cclxuLmxvZ2luQnV0dG9uIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuXHRjb2xvcjogIzg3MjNmZjtcclxuXHRib3JkZXItcmFkaXVzOiAyZW07XHJcblx0Ym9yZGVyLWNvbG9yOiB3aGl0ZTtcclxuXHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdGZvbnQtc2l6ZTogLjdlbTtcclxuXHRmb250LXdlaWdodDogYm9sZGVyO1xyXG5cdHBhZGRpbmc6IDdweCAxMnB4IDdweCAxMnB4O1xyXG5cdCY6aG92ZXIge1xyXG5cdFx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcblx0XHRjb2xvcjogIzg3MjNmZjtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDJlbTtcclxuXHRcdGJvcmRlci1jb2xvcjogd2hpdGU7XHJcblx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IGJvbGRlcjtcclxuXHRcdHBhZGRpbmc6IDdweCAxMnB4IDdweCAxMnB4O1xyXG5cdH1cclxufVxyXG4ubG9naW5QYWdlQnV0dG9uIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjODcyM2ZmO1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHRib3JkZXItcmFkaXVzOiAyZW07XHJcblx0Ym9yZGVyLWNvbG9yOiAjODcyM2ZmO1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdGZvbnQtd2VpZ2h0OiA5MDA7XHJcblx0cGFkZGluZzogMTBweCAyMHB4IDEwcHggMjBweDtcclxuXHRtYXJnaW4tdG9wOiAxM3B4O1xyXG5cdG1hcmdpbi1ib3R0b206IDI1cHg7XHJcblx0d2lkdGg6IDg3cHg7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IDI1JTtcclxuXHQmOmhvdmVyIHtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6ICM4NzIzZmY7XHJcblx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRib3JkZXItcmFkaXVzOiAyZW07XHJcblx0XHRib3JkZXItY29sb3I6ICM4NzIzZmY7XHJcblx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDkwMDtcclxuXHRcdHBhZGRpbmc6IDEwcHggMjBweCAxMHB4IDIwcHg7XHJcblx0XHRtYXJnaW4tdG9wOiAxM3B4O1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMjVweDtcclxuXHRcdHdpZHRoOiA4N3B4O1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0bGVmdDogMjUlO1xyXG5cdH1cclxufVxyXG4ubG9naW5DYXJkLmNhcmQge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0d2lkdGg6IDMyNXB4O1xyXG5cdG1pbi1oZWlnaHQ6IDM3MHB4O1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHRvcDogNDAlO1xyXG5cdGxlZnQ6IDUwJTtcclxuXHRtYXJnaW4tcmlnaHQ6IC01MCU7XHJcblx0dHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5sYWJlbCB7XHJcblx0bWFyZ2luLWJvdHRvbTogMGVtO1xyXG5cdGZvbnQtc2l6ZTogLjZlbTtcclxuXHRjb2xvcjogc2xhdGVncmF5O1xyXG5cdGZvbnQtd2VpZ2h0OiA4MDA7XHJcbn1cclxuLnRpdGxlTG9naW4ge1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRtYXJnaW4tYm90dG9tOiA0NXB4O1xyXG5cdG1hcmdpbi10b3A6IDE1cHg7XHJcblx0Zm9udC1zaXplOiAxN3B4O1xyXG5cdGZvbnQtd2VpZ2h0OiA4MDA7XHJcbn1cclxuLmZvcm1JbnB1dCB7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogI0Y1RjVGNTtcclxuXHRib3JkZXItY29sb3I6ICNGNUY1RjU7XHJcbn1cclxuLmFsbElucHV0cyB7XHJcblx0cGFkZGluZzogMHB4IDI1cHggMHB4IDI1cHg7XHJcbn1cclxuLnB3ZElucHV0IHtcclxuXHR3aWR0aDogMjczcHg7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5mYSB7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHRvcDogMjU1cHg7XHJcblx0cmlnaHQ6IDM1cHg7XHJcblx0Y29sb3I6ICM4NzIzZmY7XHJcbn1cclxufSIsIi8vIEJyZWFrcG9pbnQgdmlld3BvcnQgc2l6ZXMgYW5kIG1lZGlhIHF1ZXJpZXMuXG4vL1xuLy8gQnJlYWtwb2ludHMgYXJlIGRlZmluZWQgYXMgYSBtYXAgb2YgKG5hbWU6IG1pbmltdW0gd2lkdGgpLCBvcmRlciBmcm9tIHNtYWxsIHRvIGxhcmdlOlxuLy9cbi8vICAgICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweClcbi8vXG4vLyBUaGUgbWFwIGRlZmluZWQgaW4gdGhlIGAkZ3JpZC1icmVha3BvaW50c2AgZ2xvYmFsIHZhcmlhYmxlIGlzIHVzZWQgYXMgdGhlIGAkYnJlYWtwb2ludHNgIGFyZ3VtZW50IGJ5IGRlZmF1bHQuXG5cbi8vIE5hbWUgb2YgdGhlIG5leHQgYnJlYWtwb2ludCwgb3IgbnVsbCBmb3IgdGhlIGxhc3QgYnJlYWtwb2ludC5cbi8vXG4vLyAgICA+PiBicmVha3BvaW50LW5leHQoc20pXG4vLyAgICBtZFxuLy8gICAgPj4gYnJlYWtwb2ludC1uZXh0KHNtLCAoeHM6IDAsIHNtOiA1NzZweCwgbWQ6IDc2OHB4LCBsZzogOTkycHgsIHhsOiAxMjAwcHgpKVxuLy8gICAgbWRcbi8vICAgID4+IGJyZWFrcG9pbnQtbmV4dChzbSwgJGJyZWFrcG9pbnQtbmFtZXM6ICh4cyBzbSBtZCBsZyB4bCkpXG4vLyAgICBtZFxuQGZ1bmN0aW9uIGJyZWFrcG9pbnQtbmV4dCgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cywgJGJyZWFrcG9pbnQtbmFtZXM6IG1hcC1rZXlzKCRicmVha3BvaW50cykpIHtcbiAgJG46IGluZGV4KCRicmVha3BvaW50LW5hbWVzLCAkbmFtZSk7XG4gIEByZXR1cm4gaWYoJG4gIT0gbnVsbCBhbmQgJG4gPCBsZW5ndGgoJGJyZWFrcG9pbnQtbmFtZXMpLCBudGgoJGJyZWFrcG9pbnQtbmFtZXMsICRuICsgMSksIG51bGwpO1xufVxuXG4vLyBNaW5pbXVtIGJyZWFrcG9pbnQgd2lkdGguIE51bGwgZm9yIHRoZSBzbWFsbGVzdCAoZmlyc3QpIGJyZWFrcG9pbnQuXG4vL1xuLy8gICAgPj4gYnJlYWtwb2ludC1taW4oc20sICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweCkpXG4vLyAgICA1NzZweFxuQGZ1bmN0aW9uIGJyZWFrcG9pbnQtbWluKCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gICRtaW46IG1hcC1nZXQoJGJyZWFrcG9pbnRzLCAkbmFtZSk7XG4gIEByZXR1cm4gaWYoJG1pbiAhPSAwLCAkbWluLCBudWxsKTtcbn1cblxuLy8gTWF4aW11bSBicmVha3BvaW50IHdpZHRoLiBOdWxsIGZvciB0aGUgbGFyZ2VzdCAobGFzdCkgYnJlYWtwb2ludC5cbi8vIFRoZSBtYXhpbXVtIHZhbHVlIGlzIGNhbGN1bGF0ZWQgYXMgdGhlIG1pbmltdW0gb2YgdGhlIG5leHQgb25lIGxlc3MgMC4wMnB4XG4vLyB0byB3b3JrIGFyb3VuZCB0aGUgbGltaXRhdGlvbnMgb2YgYG1pbi1gIGFuZCBgbWF4LWAgcHJlZml4ZXMgYW5kIHZpZXdwb3J0cyB3aXRoIGZyYWN0aW9uYWwgd2lkdGhzLlxuLy8gU2VlIGh0dHBzOi8vd3d3LnczLm9yZy9UUi9tZWRpYXF1ZXJpZXMtNC8jbXEtbWluLW1heFxuLy8gVXNlcyAwLjAycHggcmF0aGVyIHRoYW4gMC4wMXB4IHRvIHdvcmsgYXJvdW5kIGEgY3VycmVudCByb3VuZGluZyBidWcgaW4gU2FmYXJpLlxuLy8gU2VlIGh0dHBzOi8vYnVncy53ZWJraXQub3JnL3Nob3dfYnVnLmNnaT9pZD0xNzgyNjFcbi8vXG4vLyAgICA+PiBicmVha3BvaW50LW1heChzbSwgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIDc2Ny45OHB4XG5AZnVuY3Rpb24gYnJlYWtwb2ludC1tYXgoJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG5leHQ6IGJyZWFrcG9pbnQtbmV4dCgkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQHJldHVybiBpZigkbmV4dCwgYnJlYWtwb2ludC1taW4oJG5leHQsICRicmVha3BvaW50cykgLSAuMDIsIG51bGwpO1xufVxuXG4vLyBSZXR1cm5zIGEgYmxhbmsgc3RyaW5nIGlmIHNtYWxsZXN0IGJyZWFrcG9pbnQsIG90aGVyd2lzZSByZXR1cm5zIHRoZSBuYW1lIHdpdGggYSBkYXNoIGluIGZyb250LlxuLy8gVXNlZnVsIGZvciBtYWtpbmcgcmVzcG9uc2l2ZSB1dGlsaXRpZXMuXG4vL1xuLy8gICAgPj4gYnJlYWtwb2ludC1pbmZpeCh4cywgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIFwiXCIgIChSZXR1cm5zIGEgYmxhbmsgc3RyaW5nKVxuLy8gICAgPj4gYnJlYWtwb2ludC1pbmZpeChzbSwgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIFwiLXNtXCJcbkBmdW5jdGlvbiBicmVha3BvaW50LWluZml4KCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gIEByZXR1cm4gaWYoYnJlYWtwb2ludC1taW4oJG5hbWUsICRicmVha3BvaW50cykgPT0gbnVsbCwgXCJcIiwgXCItI3skbmFtZX1cIik7XG59XG5cbi8vIE1lZGlhIG9mIGF0IGxlYXN0IHRoZSBtaW5pbXVtIGJyZWFrcG9pbnQgd2lkdGguIE5vIHF1ZXJ5IGZvciB0aGUgc21hbGxlc3QgYnJlYWtwb2ludC5cbi8vIE1ha2VzIHRoZSBAY29udGVudCBhcHBseSB0byB0aGUgZ2l2ZW4gYnJlYWtwb2ludCBhbmQgd2lkZXIuXG5AbWl4aW4gbWVkaWEtYnJlYWtwb2ludC11cCgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWluOiBicmVha3BvaW50LW1pbigkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQGlmICRtaW4ge1xuICAgIEBtZWRpYSAobWluLXdpZHRoOiAkbWluKSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8vIE1lZGlhIG9mIGF0IG1vc3QgdGhlIG1heGltdW0gYnJlYWtwb2ludCB3aWR0aC4gTm8gcXVlcnkgZm9yIHRoZSBsYXJnZXN0IGJyZWFrcG9pbnQuXG4vLyBNYWtlcyB0aGUgQGNvbnRlbnQgYXBwbHkgdG8gdGhlIGdpdmVuIGJyZWFrcG9pbnQgYW5kIG5hcnJvd2VyLlxuQG1peGluIG1lZGlhLWJyZWFrcG9pbnQtZG93bigkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWF4OiBicmVha3BvaW50LW1heCgkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgQGlmICRtYXgge1xuICAgIEBtZWRpYSAobWF4LXdpZHRoOiAkbWF4KSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2Uge1xuICAgIEBjb250ZW50O1xuICB9XG59XG5cbi8vIE1lZGlhIHRoYXQgc3BhbnMgbXVsdGlwbGUgYnJlYWtwb2ludCB3aWR0aHMuXG4vLyBNYWtlcyB0aGUgQGNvbnRlbnQgYXBwbHkgYmV0d2VlbiB0aGUgbWluIGFuZCBtYXggYnJlYWtwb2ludHNcbkBtaXhpbiBtZWRpYS1icmVha3BvaW50LWJldHdlZW4oJGxvd2VyLCAkdXBwZXIsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG1pbjogYnJlYWtwb2ludC1taW4oJGxvd2VyLCAkYnJlYWtwb2ludHMpO1xuICAkbWF4OiBicmVha3BvaW50LW1heCgkdXBwZXIsICRicmVha3BvaW50cyk7XG5cbiAgQGlmICRtaW4gIT0gbnVsbCBhbmQgJG1heCAhPSBudWxsIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogJG1pbikgYW5kIChtYXgtd2lkdGg6ICRtYXgpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWF4ID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtdXAoJGxvd2VyLCAkYnJlYWtwb2ludHMpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWluID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtZG93bigkdXBwZXIsICRicmVha3BvaW50cykge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9XG59XG5cbi8vIE1lZGlhIGJldHdlZW4gdGhlIGJyZWFrcG9pbnQncyBtaW5pbXVtIGFuZCBtYXhpbXVtIHdpZHRocy5cbi8vIE5vIG1pbmltdW0gZm9yIHRoZSBzbWFsbGVzdCBicmVha3BvaW50LCBhbmQgbm8gbWF4aW11bSBmb3IgdGhlIGxhcmdlc3Qgb25lLlxuLy8gTWFrZXMgdGhlIEBjb250ZW50IGFwcGx5IG9ubHkgdG8gdGhlIGdpdmVuIGJyZWFrcG9pbnQsIG5vdCB2aWV3cG9ydHMgYW55IHdpZGVyIG9yIG5hcnJvd2VyLlxuQG1peGluIG1lZGlhLWJyZWFrcG9pbnQtb25seSgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWluOiBicmVha3BvaW50LW1pbigkbmFtZSwgJGJyZWFrcG9pbnRzKTtcbiAgJG1heDogYnJlYWtwb2ludC1tYXgoJG5hbWUsICRicmVha3BvaW50cyk7XG5cbiAgQGlmICRtaW4gIT0gbnVsbCBhbmQgJG1heCAhPSBudWxsIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogJG1pbikgYW5kIChtYXgtd2lkdGg6ICRtYXgpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWF4ID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtdXAoJG5hbWUsICRicmVha3BvaW50cykge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9IEBlbHNlIGlmICRtaW4gPT0gbnVsbCB7XG4gICAgQGluY2x1ZGUgbWVkaWEtYnJlYWtwb2ludC1kb3duKCRuYW1lLCAkYnJlYWtwb2ludHMpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuIl19 */");

/***/ }),

/***/ "./src/app/home-page/home-page.component.ts":
/*!**************************************************!*\
  !*** ./src/app/home-page/home-page.component.ts ***!
  \**************************************************/
/*! exports provided: HomePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageComponent", function() { return HomePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomePageComponent = /** @class */ (function () {
    function HomePageComponent() {
        this.loginStatus = false;
    }
    HomePageComponent.prototype.ngOnInit = function () {
    };
    HomePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-page',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-page/home-page.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home-page.component.scss */ "./src/app/home-page/home-page.component.scss")).default]
        })
    ], HomePageComponent);
    return HomePageComponent;
}());



/***/ }),

/***/ "./src/app/login-page/login-page.component.scss":
/*!******************************************************!*\
  !*** ./src/app/login-page/login-page.component.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\nbody {\n  display: -webkit-box;\n  display: flex;\n  font-family: Muli,sans-serif;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  background-color: none; }\nbody > div > a {\n    background-color: none; }\nbody:before {\n    content: '';\n    z-index: 0;\n    position: absolute;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    -webkit-filter: blur(2px);\n    filter: brightness(40%);\n    background: url('macbook wallpaper.jpg') no-repeat;\n    background-size: cover;\n    display: -webkit-box;\n    display: flex;\n    font-family: Muli,sans-serif;\n    -webkit-box-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n            justify-content: center; }\nbody h6 {\n    padding: 10px;\n    color: white;\n    float: right; }\n.btnDisabled {\n  color: #fff;\n  background-color: #6c757d;\n  border-color: #6c757d; }\n.contentHeading {\n  text-align: center;\n  color: white;\n  font-size: 2.5em;\n  padding-bottom: 15px; }\nh1 {\n  color: navy;\n  margin-left: 20px; }\n.footer {\n  position: fixed;\n  left: 0;\n  bottom: 0;\n  padding: 1em;\n  background-color: white;\n  color: slategray;\n  text-align: left;\n  display: inline;\n  font-size: .7em;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -webkit-box-align: center;\n          align-items: center; }\n.footerElement {\n  border-right: 1px solid #b4b4b4;\n  padding: 2px 4px;\n  margin: 0;\n  color: #696969; }\n.background {\n  -webkit-filter: blur(5px);\n          filter: blur(5px);\n  -webkit-filter: brightness(100%);\n          filter: brightness(100%); }\n.pageContent {\n  height: inherit;\n  display: -webkit-box;\n  display: flex;\n  background: none;\n  border: none;\n  text-align: center;\n  color: white;\n  font-family: Muli,sans-serif;\n  -webkit-box-align: center;\n          align-items: center;\n  -webkit-box-pack: start;\n          justify-content: flex-start;\n  align-self: center;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column; }\n.loginPageButton {\n  background-color: #8723ff;\n  color: white;\n  border-radius: 2em;\n  border-color: #8723ff;\n  font-family: Muli,sans-serif;\n  font-size: .7em;\n  font-weight: 900;\n  padding: 10px 20px 10px 20px;\n  margin-bottom: 25px;\n  width: 87px;\n  position: absolute;\n  left: 25%;\n  margin-top: 22px;\n  margin-left: 34px; }\n.loginPageButton:hover {\n    background-color: #8723ff;\n    color: white;\n    border-radius: 2em;\n    border-color: #8723ff;\n    font-family: Muli,sans-serif;\n    font-size: .7em;\n    font-weight: 900;\n    padding: 10px 20px 10px 20px;\n    margin-bottom: 25px;\n    width: 87px;\n    position: absolute;\n    left: 25%;\n    margin-top: 22px;\n    margin-left: 34px; }\n.welcomeHead {\n  align-self: center;\n  padding-bottom: 200‒;\n  color: white;\n  padding-bottom: 20px;\n  font-size: 2em;\n  font-weight: lighter;\n  font-family: Muli,sans-serif; }\n.loginCard {\n  display: -webkit-box;\n  display: flex;\n  min-height: 285px;\n  font-family: Muli,sans-serif;\n  align-self: center;\n  height: auto;\n  width: 297px; }\n@media (min-width: 576px) {\n    .loginCard {\n      width: 325px; } }\n.invalidCreds {\n  color: red;\n  font-size: small;\n  text-align: center; }\nlabel {\n  margin-bottom: 0em;\n  font-size: .6em;\n  color: slategray;\n  font-weight: 800; }\n.titleLogin {\n  text-align: center;\n  margin-bottom: 25px;\n  margin-top: 15px;\n  font-size: 17px;\n  font-weight: 800; }\n.formInput {\n  font-size: 12px;\n  font-family: Muli,sans-serif;\n  background-color: #F5F5F5;\n  border-color: #F5F5F5; }\n.allInputs {\n  padding: 0px 25px 0px 25px; }\n.pwdInput {\n  display: inline-block; }\n.fa {\n  position: relative;\n  top: -25px;\n  right: 10px;\n  color: #8723ff; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4tcGFnZS9sb2dpbi1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sb2dpbi1wYWdlL0M6XFxQYXdhblxcd2ViYXBwL3NyY1xcYXBwXFxsb2dpbi1wYWdlXFxsb2dpbi1wYWdlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sb2dpbi1wYWdlL0M6XFxQYXdhblxcd2ViYXBwL25vZGVfbW9kdWxlc1xcYm9vdHN0cmFwXFxzY3NzXFxtaXhpbnNcXF9icmVha3BvaW50cy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0loQjtFQUNDLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUE0QjtFQUM1Qiw0QkFBc0I7RUFBdEIsNkJBQXNCO1VBQXRCLHNCQUFzQjtFQUN0Qix5QkFBOEI7VUFBOUIsOEJBQThCO0VBQzlCLHNCQUFzQixFQUFBO0FBTHZCO0lBUUcsc0JBQXNCLEVBQUE7QUFSekI7SUFZRSxXQUFXO0lBQ1gsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixPQUFPO0lBQ1AsUUFBUTtJQUNSLE1BQU07SUFDTixTQUFTO0lBQ1QseUJBQXlCO0lBQ3pCLHVCQUF1QjtJQUN2QixrREFBK0Q7SUFDL0Qsc0JBQXNCO0lBQ3RCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLDRCQUE0QjtJQUM1Qix5QkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLHdCQUF1QjtZQUF2Qix1QkFBdUIsRUFBQTtBQTFCekI7SUE2QkMsYUFBWTtJQUNaLFlBQVc7SUFDWCxZQUNBLEVBQUE7QUFHRDtFQUNBLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7QUFHckI7RUFDQyxrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLGdCQUFnQjtFQUNoQixvQkFBb0IsRUFBQTtBQUVyQjtFQUNDLFdBQVc7RUFDWCxpQkFBaUIsRUFBQTtBQUVsQjtFQUNDLGVBQWU7RUFDZixPQUFPO0VBQ1AsU0FBUztFQUNULFlBQVk7RUFDWix1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsZUFBZTtFQUNmLG9CQUFhO0VBQWIsYUFBYTtFQUNiLHlCQUE4QjtVQUE5Qiw4QkFBOEI7RUFDOUIseUJBQW1CO1VBQW5CLG1CQUFtQixFQUFBO0FBRXBCO0VBQ0MsK0JBQStCO0VBQy9CLGdCQUFnQjtFQUNoQixTQUFTO0VBQ1QsY0FBYyxFQUFBO0FBRWY7RUFDQyx5QkFBaUI7VUFBakIsaUJBQWlCO0VBQ2pCLGdDQUF3QjtVQUF4Qix3QkFBd0IsRUFBQTtBQUV6QjtFQUNDLGVBQWU7RUFDZixvQkFBYTtFQUFiLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osNEJBQTRCO0VBQzVCLHlCQUFtQjtVQUFuQixtQkFBbUI7RUFDbkIsdUJBQTJCO1VBQTNCLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0IsRUFBQTtBQUV2QjtFQUNDLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHFCQUFxQjtFQUNyQiw0QkFBNEI7RUFDNUIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw0QkFBNEI7RUFDNUIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsU0FBUztFQUNULGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTtBQWRsQjtJQWdCRSx5QkFBeUI7SUFDekIsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsNEJBQTRCO0lBQzVCLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsNEJBQTRCO0lBQzVCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxnQkFBZ0I7SUFDaEIsaUJBQWlCLEVBQUE7QUFHbkI7RUFDQyxrQkFBa0I7RUFDbEIsb0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixvQkFBb0I7RUFDcEIsY0FBYztFQUNkLG9CQUFvQjtFQUNwQiw0QkFBNEIsRUFBQTtBQUU1QjtFQUNBLG9CQUFhO0VBQWIsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQiw0QkFBNEI7RUFDNUIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixZQUFZLEVBQUE7QUMvRVQ7SUR5RUg7TUFRRSxZQUFZLEVBQUEsRUFHZDtBQUNEO0VBQ0MsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixrQkFBa0IsRUFBQTtBQUVuQjtFQUNDLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBO0FBRWpCO0VBQ0Msa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGdCQUFnQixFQUFBO0FBRWpCO0VBQ0MsZUFBZTtFQUNmLDRCQUE0QjtFQUM1Qix5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7QUFFdEI7RUFDQywwQkFBMEIsRUFBQTtBQUUzQjtFQUNDLHFCQUFxQixFQUFBO0FBRXRCO0VBQ0Msa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixXQUFXO0VBQ1gsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbG9naW4tcGFnZS9sb2dpbi1wYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuYm9keSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYmFja2dyb3VuZC1jb2xvcjogbm9uZTsgfVxuICBib2R5ID4gZGl2ID4gYSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogbm9uZTsgfVxuICBib2R5OmJlZm9yZSB7XG4gICAgY29udGVudDogJyc7XG4gICAgei1pbmRleDogMDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIC13ZWJraXQtZmlsdGVyOiBibHVyKDJweCk7XG4gICAgZmlsdGVyOiBicmlnaHRuZXNzKDQwJSk7XG4gICAgYmFja2dyb3VuZDogdXJsKFwic3JjXFxcXGFzc2V0c1xcXFxtYWNib29rIHdhbGxwYXBlci5qcGdcIikgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7IH1cbiAgYm9keSBoNiB7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZmxvYXQ6IHJpZ2h0OyB9XG5cbi5idG5EaXNhYmxlZCB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNmM3NTdkO1xuICBib3JkZXItY29sb3I6ICM2Yzc1N2Q7IH1cblxuLmNvbnRlbnRIZWFkaW5nIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMi41ZW07XG4gIHBhZGRpbmctYm90dG9tOiAxNXB4OyB9XG5cbmgxIHtcbiAgY29sb3I6IG5hdnk7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4OyB9XG5cbi5mb290ZXIge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcGFkZGluZzogMWVtO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgY29sb3I6IHNsYXRlZ3JheTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZGlzcGxheTogaW5saW5lO1xuICBmb250LXNpemU6IC43ZW07XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjsgfVxuXG4uZm9vdGVyRWxlbWVudCB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNiNGI0YjQ7XG4gIHBhZGRpbmc6IDJweCA0cHg7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6ICM2OTY5Njk7IH1cblxuLmJhY2tncm91bmQge1xuICBmaWx0ZXI6IGJsdXIoNXB4KTtcbiAgZmlsdGVyOiBicmlnaHRuZXNzKDEwMCUpOyB9XG5cbi5wYWdlQ29udGVudCB7XG4gIGhlaWdodDogaW5oZXJpdDtcbiAgZGlzcGxheTogZmxleDtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiBub25lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47IH1cblxuLmxvZ2luUGFnZUJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM4NzIzZmY7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMmVtO1xuICBib3JkZXItY29sb3I6ICM4NzIzZmY7XG4gIGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogLjdlbTtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgcGFkZGluZzogMTBweCAyMHB4IDEwcHggMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweDtcbiAgd2lkdGg6IDg3cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjUlO1xuICBtYXJnaW4tdG9wOiAyMnB4O1xuICBtYXJnaW4tbGVmdDogMzRweDsgfVxuICAubG9naW5QYWdlQnV0dG9uOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjODcyM2ZmO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBib3JkZXItcmFkaXVzOiAyZW07XG4gICAgYm9yZGVyLWNvbG9yOiAjODcyM2ZmO1xuICAgIGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XG4gICAgZm9udC1zaXplOiAuN2VtO1xuICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gICAgcGFkZGluZzogMTBweCAyMHB4IDEwcHggMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xuICAgIHdpZHRoOiA4N3B4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiAyNSU7XG4gICAgbWFyZ2luLXRvcDogMjJweDtcbiAgICBtYXJnaW4tbGVmdDogMzRweDsgfVxuXG4ud2VsY29tZUhlYWQge1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gIHBhZGRpbmctYm90dG9tOiAyMDDigJI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIGZvbnQtc2l6ZTogMmVtO1xuICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgZm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjsgfVxuXG4ubG9naW5DYXJkIHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWluLWhlaWdodDogMjg1cHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMjk3cHg7IH1cbiAgQG1lZGlhIChtaW4td2lkdGg6IDU3NnB4KSB7XG4gICAgLmxvZ2luQ2FyZCB7XG4gICAgICB3aWR0aDogMzI1cHg7IH0gfVxuXG4uaW52YWxpZENyZWRzIHtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiBzbWFsbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbmxhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMGVtO1xuICBmb250LXNpemU6IC42ZW07XG4gIGNvbG9yOiBzbGF0ZWdyYXk7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7IH1cblxuLnRpdGxlTG9naW4ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDsgfVxuXG4uZm9ybUlucHV0IHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjVGNUY1O1xuICBib3JkZXItY29sb3I6ICNGNUY1RjU7IH1cblxuLmFsbElucHV0cyB7XG4gIHBhZGRpbmc6IDBweCAyNXB4IDBweCAyNXB4OyB9XG5cbi5wd2RJbnB1dCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jazsgfVxuXG4uZmEge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogLTI1cHg7XG4gIHJpZ2h0OiAxMHB4O1xuICBjb2xvcjogIzg3MjNmZjsgfVxuIiwiQGltcG9ydCBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9fZnVuY3Rpb25zLnNjc3NcIjtcclxuQGltcG9ydCBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9fdmFyaWFibGVzLnNjc3NcIjtcclxuQGltcG9ydCBcIi4uLy4uLy4uL25vZGVfbW9kdWxlcy9ib290c3RyYXAvc2Nzcy9taXhpbnMvX2JyZWFrcG9pbnRzLnNjc3NcIjtcclxuXHJcbmJvZHkge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0Zm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcclxuXHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiBub25lO1xyXG5cdD5kaXYge1xyXG5cdFx0PmEge1xyXG5cdFx0XHRiYWNrZ3JvdW5kLWNvbG9yOiBub25lO1xyXG5cdFx0fVxyXG5cdH1cclxuXHQmOmJlZm9yZSB7XHJcblx0XHRjb250ZW50OiAnJztcclxuXHRcdHotaW5kZXg6IDA7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRsZWZ0OiAwO1xyXG5cdFx0cmlnaHQ6IDA7XHJcblx0XHR0b3A6IDA7XHJcblx0XHRib3R0b206IDA7XHJcblx0XHQtd2Via2l0LWZpbHRlcjogYmx1cigycHgpO1xyXG5cdFx0ZmlsdGVyOiBicmlnaHRuZXNzKDQwJSk7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoJ3NyY1xcXFxhc3NldHNcXFxcbWFjYm9vayB3YWxscGFwZXIuanBnJykgbm8tcmVwZWF0O1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdH1cclxuXHRoNntcclxuXHRwYWRkaW5nOjEwcHg7XHJcblx0Y29sb3I6d2hpdGU7XHJcblx0ZmxvYXQ6cmlnaHRcclxuXHR9XHJcbn1cclxuXHJcbi5idG5EaXNhYmxlZHsgICAgXHJcbmNvbG9yOiAjZmZmO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjNmM3NTdkO1xyXG5ib3JkZXItY29sb3I6ICM2Yzc1N2Q7XHJcbn1cclxuXHJcbi5jb250ZW50SGVhZGluZyB7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHRmb250LXNpemU6IDIuNWVtO1xyXG5cdHBhZGRpbmctYm90dG9tOiAxNXB4O1xyXG59XHJcbmgxIHtcclxuXHRjb2xvcjogbmF2eTtcclxuXHRtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG4uZm9vdGVyIHtcclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcblx0bGVmdDogMDtcclxuXHRib3R0b206IDA7XHJcblx0cGFkZGluZzogMWVtO1xyXG5cdGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5cdGNvbG9yOiBzbGF0ZWdyYXk7XHJcblx0dGV4dC1hbGlnbjogbGVmdDtcclxuXHRkaXNwbGF5OiBpbmxpbmU7XHJcblx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0anVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLmZvb3RlckVsZW1lbnQge1xyXG5cdGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNiNGI0YjQ7XHJcblx0cGFkZGluZzogMnB4IDRweDtcclxuXHRtYXJnaW46IDA7XHJcblx0Y29sb3I6ICM2OTY5Njk7XHJcbn1cclxuLmJhY2tncm91bmQge1xyXG5cdGZpbHRlcjogYmx1cig1cHgpO1xyXG5cdGZpbHRlcjogYnJpZ2h0bmVzcygxMDAlKTtcclxufVxyXG4ucGFnZUNvbnRlbnQge1xyXG5cdGhlaWdodDogaW5oZXJpdDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGJhY2tncm91bmQ6IG5vbmU7XHJcblx0Ym9yZGVyOiBub25lO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRjb2xvcjogd2hpdGU7XHJcblx0Zm9udC1mYW1pbHk6IE11bGksc2Fucy1zZXJpZjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuXHRhbGlnbi1zZWxmOiBjZW50ZXI7XHJcblx0ZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG4ubG9naW5QYWdlQnV0dG9uIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjODcyM2ZmO1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHRib3JkZXItcmFkaXVzOiAyZW07XHJcblx0Ym9yZGVyLWNvbG9yOiAjODcyM2ZmO1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdGZvbnQtd2VpZ2h0OiA5MDA7XHJcblx0cGFkZGluZzogMTBweCAyMHB4IDEwcHggMjBweDtcclxuXHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cdHdpZHRoOiA4N3B4O1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRsZWZ0OiAyNSU7XHJcblx0bWFyZ2luLXRvcDogMjJweDtcclxuXHRtYXJnaW4tbGVmdDogMzRweDtcclxuXHQmOmhvdmVyIHtcclxuXHRcdGJhY2tncm91bmQtY29sb3I6ICM4NzIzZmY7XHJcblx0XHRjb2xvcjogd2hpdGU7XHJcblx0XHRib3JkZXItcmFkaXVzOiAyZW07XHJcblx0XHRib3JkZXItY29sb3I6ICM4NzIzZmY7XHJcblx0XHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdFx0Zm9udC1zaXplOiAuN2VtO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDkwMDtcclxuXHRcdHBhZGRpbmc6IDEwcHggMjBweCAxMHB4IDIwcHg7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cdFx0d2lkdGg6IDg3cHg7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRsZWZ0OiAyNSU7XHJcblx0XHRtYXJnaW4tdG9wOiAyMnB4O1xyXG5cdFx0bWFyZ2luLWxlZnQ6IDM0cHg7XHJcblx0fVxyXG59XHJcbi53ZWxjb21lSGVhZCB7XHJcblx0YWxpZ24tc2VsZjogY2VudGVyO1xyXG5cdHBhZGRpbmctYm90dG9tOiAyMDDigJI7XHJcblx0Y29sb3I6IHdoaXRlO1xyXG5cdHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG5cdGZvbnQtc2l6ZTogMmVtO1xyXG5cdGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcbn1cclxuIC5sb2dpbkNhcmR7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRtaW4taGVpZ2h0OiAyODVweDtcclxuXHRmb250LWZhbWlseTogTXVsaSxzYW5zLXNlcmlmO1xyXG5cdGFsaWduLXNlbGY6IGNlbnRlcjtcclxuXHRoZWlnaHQ6IGF1dG87XHJcblx0d2lkdGg6IDI5N3B4O1xyXG5cdEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtdXAoc20pIHsgXHJcblx0ICB3aWR0aDogMzI1cHg7XHJcblx0ICBcclxuICAgICB9XHJcbn1cclxuLmludmFsaWRDcmVkcyB7XHJcblx0Y29sb3I6IHJlZDtcclxuXHRmb250LXNpemU6IHNtYWxsO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5sYWJlbCB7XHJcblx0bWFyZ2luLWJvdHRvbTogMGVtO1xyXG5cdGZvbnQtc2l6ZTogLjZlbTtcclxuXHRjb2xvcjogc2xhdGVncmF5O1xyXG5cdGZvbnQtd2VpZ2h0OiA4MDA7XHJcbn1cclxuLnRpdGxlTG9naW4ge1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cdG1hcmdpbi10b3A6IDE1cHg7XHJcblx0Zm9udC1zaXplOiAxN3B4O1xyXG5cdGZvbnQtd2VpZ2h0OiA4MDA7XHJcbn1cclxuLmZvcm1JbnB1dCB7XHJcblx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdGZvbnQtZmFtaWx5OiBNdWxpLHNhbnMtc2VyaWY7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogI0Y1RjVGNTtcclxuXHRib3JkZXItY29sb3I6ICNGNUY1RjU7XHJcbn1cclxuLmFsbElucHV0cyB7XHJcblx0cGFkZGluZzogMHB4IDI1cHggMHB4IDI1cHg7XHJcbn1cclxuLnB3ZElucHV0IHtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuLmZhIHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0dG9wOiAtMjVweDtcclxuXHRyaWdodDogMTBweDtcclxuXHRjb2xvcjogIzg3MjNmZjtcclxufVxyXG4iLCIvLyBCcmVha3BvaW50IHZpZXdwb3J0IHNpemVzIGFuZCBtZWRpYSBxdWVyaWVzLlxuLy9cbi8vIEJyZWFrcG9pbnRzIGFyZSBkZWZpbmVkIGFzIGEgbWFwIG9mIChuYW1lOiBtaW5pbXVtIHdpZHRoKSwgb3JkZXIgZnJvbSBzbWFsbCB0byBsYXJnZTpcbi8vXG4vLyAgICAoeHM6IDAsIHNtOiA1NzZweCwgbWQ6IDc2OHB4LCBsZzogOTkycHgsIHhsOiAxMjAwcHgpXG4vL1xuLy8gVGhlIG1hcCBkZWZpbmVkIGluIHRoZSBgJGdyaWQtYnJlYWtwb2ludHNgIGdsb2JhbCB2YXJpYWJsZSBpcyB1c2VkIGFzIHRoZSBgJGJyZWFrcG9pbnRzYCBhcmd1bWVudCBieSBkZWZhdWx0LlxuXG4vLyBOYW1lIG9mIHRoZSBuZXh0IGJyZWFrcG9pbnQsIG9yIG51bGwgZm9yIHRoZSBsYXN0IGJyZWFrcG9pbnQuXG4vL1xuLy8gICAgPj4gYnJlYWtwb2ludC1uZXh0KHNtKVxuLy8gICAgbWRcbi8vICAgID4+IGJyZWFrcG9pbnQtbmV4dChzbSwgKHhzOiAwLCBzbTogNTc2cHgsIG1kOiA3NjhweCwgbGc6IDk5MnB4LCB4bDogMTIwMHB4KSlcbi8vICAgIG1kXG4vLyAgICA+PiBicmVha3BvaW50LW5leHQoc20sICRicmVha3BvaW50LW5hbWVzOiAoeHMgc20gbWQgbGcgeGwpKVxuLy8gICAgbWRcbkBmdW5jdGlvbiBicmVha3BvaW50LW5leHQoJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMsICRicmVha3BvaW50LW5hbWVzOiBtYXAta2V5cygkYnJlYWtwb2ludHMpKSB7XG4gICRuOiBpbmRleCgkYnJlYWtwb2ludC1uYW1lcywgJG5hbWUpO1xuICBAcmV0dXJuIGlmKCRuICE9IG51bGwgYW5kICRuIDwgbGVuZ3RoKCRicmVha3BvaW50LW5hbWVzKSwgbnRoKCRicmVha3BvaW50LW5hbWVzLCAkbiArIDEpLCBudWxsKTtcbn1cblxuLy8gTWluaW11bSBicmVha3BvaW50IHdpZHRoLiBOdWxsIGZvciB0aGUgc21hbGxlc3QgKGZpcnN0KSBicmVha3BvaW50LlxuLy9cbi8vICAgID4+IGJyZWFrcG9pbnQtbWluKHNtLCAoeHM6IDAsIHNtOiA1NzZweCwgbWQ6IDc2OHB4LCBsZzogOTkycHgsIHhsOiAxMjAwcHgpKVxuLy8gICAgNTc2cHhcbkBmdW5jdGlvbiBicmVha3BvaW50LW1pbigkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICAkbWluOiBtYXAtZ2V0KCRicmVha3BvaW50cywgJG5hbWUpO1xuICBAcmV0dXJuIGlmKCRtaW4gIT0gMCwgJG1pbiwgbnVsbCk7XG59XG5cbi8vIE1heGltdW0gYnJlYWtwb2ludCB3aWR0aC4gTnVsbCBmb3IgdGhlIGxhcmdlc3QgKGxhc3QpIGJyZWFrcG9pbnQuXG4vLyBUaGUgbWF4aW11bSB2YWx1ZSBpcyBjYWxjdWxhdGVkIGFzIHRoZSBtaW5pbXVtIG9mIHRoZSBuZXh0IG9uZSBsZXNzIDAuMDJweFxuLy8gdG8gd29yayBhcm91bmQgdGhlIGxpbWl0YXRpb25zIG9mIGBtaW4tYCBhbmQgYG1heC1gIHByZWZpeGVzIGFuZCB2aWV3cG9ydHMgd2l0aCBmcmFjdGlvbmFsIHdpZHRocy5cbi8vIFNlZSBodHRwczovL3d3dy53My5vcmcvVFIvbWVkaWFxdWVyaWVzLTQvI21xLW1pbi1tYXhcbi8vIFVzZXMgMC4wMnB4IHJhdGhlciB0aGFuIDAuMDFweCB0byB3b3JrIGFyb3VuZCBhIGN1cnJlbnQgcm91bmRpbmcgYnVnIGluIFNhZmFyaS5cbi8vIFNlZSBodHRwczovL2J1Z3Mud2Via2l0Lm9yZy9zaG93X2J1Zy5jZ2k/aWQ9MTc4MjYxXG4vL1xuLy8gICAgPj4gYnJlYWtwb2ludC1tYXgoc20sICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweCkpXG4vLyAgICA3NjcuOThweFxuQGZ1bmN0aW9uIGJyZWFrcG9pbnQtbWF4KCRuYW1lLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gICRuZXh0OiBicmVha3BvaW50LW5leHQoJG5hbWUsICRicmVha3BvaW50cyk7XG4gIEByZXR1cm4gaWYoJG5leHQsIGJyZWFrcG9pbnQtbWluKCRuZXh0LCAkYnJlYWtwb2ludHMpIC0gLjAyLCBudWxsKTtcbn1cblxuLy8gUmV0dXJucyBhIGJsYW5rIHN0cmluZyBpZiBzbWFsbGVzdCBicmVha3BvaW50LCBvdGhlcndpc2UgcmV0dXJucyB0aGUgbmFtZSB3aXRoIGEgZGFzaCBpbiBmcm9udC5cbi8vIFVzZWZ1bCBmb3IgbWFraW5nIHJlc3BvbnNpdmUgdXRpbGl0aWVzLlxuLy9cbi8vICAgID4+IGJyZWFrcG9pbnQtaW5maXgoeHMsICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweCkpXG4vLyAgICBcIlwiICAoUmV0dXJucyBhIGJsYW5rIHN0cmluZylcbi8vICAgID4+IGJyZWFrcG9pbnQtaW5maXgoc20sICh4czogMCwgc206IDU3NnB4LCBtZDogNzY4cHgsIGxnOiA5OTJweCwgeGw6IDEyMDBweCkpXG4vLyAgICBcIi1zbVwiXG5AZnVuY3Rpb24gYnJlYWtwb2ludC1pbmZpeCgkbmFtZSwgJGJyZWFrcG9pbnRzOiAkZ3JpZC1icmVha3BvaW50cykge1xuICBAcmV0dXJuIGlmKGJyZWFrcG9pbnQtbWluKCRuYW1lLCAkYnJlYWtwb2ludHMpID09IG51bGwsIFwiXCIsIFwiLSN7JG5hbWV9XCIpO1xufVxuXG4vLyBNZWRpYSBvZiBhdCBsZWFzdCB0aGUgbWluaW11bSBicmVha3BvaW50IHdpZHRoLiBObyBxdWVyeSBmb3IgdGhlIHNtYWxsZXN0IGJyZWFrcG9pbnQuXG4vLyBNYWtlcyB0aGUgQGNvbnRlbnQgYXBwbHkgdG8gdGhlIGdpdmVuIGJyZWFrcG9pbnQgYW5kIHdpZGVyLlxuQG1peGluIG1lZGlhLWJyZWFrcG9pbnQtdXAoJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG1pbjogYnJlYWtwb2ludC1taW4oJG5hbWUsICRicmVha3BvaW50cyk7XG4gIEBpZiAkbWluIHtcbiAgICBAbWVkaWEgKG1pbi13aWR0aDogJG1pbikge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9IEBlbHNlIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG4vLyBNZWRpYSBvZiBhdCBtb3N0IHRoZSBtYXhpbXVtIGJyZWFrcG9pbnQgd2lkdGguIE5vIHF1ZXJ5IGZvciB0aGUgbGFyZ2VzdCBicmVha3BvaW50LlxuLy8gTWFrZXMgdGhlIEBjb250ZW50IGFwcGx5IHRvIHRoZSBnaXZlbiBicmVha3BvaW50IGFuZCBuYXJyb3dlci5cbkBtaXhpbiBtZWRpYS1icmVha3BvaW50LWRvd24oJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG1heDogYnJlYWtwb2ludC1tYXgoJG5hbWUsICRicmVha3BvaW50cyk7XG4gIEBpZiAkbWF4IHtcbiAgICBAbWVkaWEgKG1heC13aWR0aDogJG1heCkge1xuICAgICAgQGNvbnRlbnQ7XG4gICAgfVxuICB9IEBlbHNlIHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuXG4vLyBNZWRpYSB0aGF0IHNwYW5zIG11bHRpcGxlIGJyZWFrcG9pbnQgd2lkdGhzLlxuLy8gTWFrZXMgdGhlIEBjb250ZW50IGFwcGx5IGJldHdlZW4gdGhlIG1pbiBhbmQgbWF4IGJyZWFrcG9pbnRzXG5AbWl4aW4gbWVkaWEtYnJlYWtwb2ludC1iZXR3ZWVuKCRsb3dlciwgJHVwcGVyLCAkYnJlYWtwb2ludHM6ICRncmlkLWJyZWFrcG9pbnRzKSB7XG4gICRtaW46IGJyZWFrcG9pbnQtbWluKCRsb3dlciwgJGJyZWFrcG9pbnRzKTtcbiAgJG1heDogYnJlYWtwb2ludC1tYXgoJHVwcGVyLCAkYnJlYWtwb2ludHMpO1xuXG4gIEBpZiAkbWluICE9IG51bGwgYW5kICRtYXggIT0gbnVsbCB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICRtaW4pIGFuZCAobWF4LXdpZHRoOiAkbWF4KSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2UgaWYgJG1heCA9PSBudWxsIHtcbiAgICBAaW5jbHVkZSBtZWRpYS1icmVha3BvaW50LXVwKCRsb3dlciwgJGJyZWFrcG9pbnRzKSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2UgaWYgJG1pbiA9PSBudWxsIHtcbiAgICBAaW5jbHVkZSBtZWRpYS1icmVha3BvaW50LWRvd24oJHVwcGVyLCAkYnJlYWtwb2ludHMpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfVxufVxuXG4vLyBNZWRpYSBiZXR3ZWVuIHRoZSBicmVha3BvaW50J3MgbWluaW11bSBhbmQgbWF4aW11bSB3aWR0aHMuXG4vLyBObyBtaW5pbXVtIGZvciB0aGUgc21hbGxlc3QgYnJlYWtwb2ludCwgYW5kIG5vIG1heGltdW0gZm9yIHRoZSBsYXJnZXN0IG9uZS5cbi8vIE1ha2VzIHRoZSBAY29udGVudCBhcHBseSBvbmx5IHRvIHRoZSBnaXZlbiBicmVha3BvaW50LCBub3Qgdmlld3BvcnRzIGFueSB3aWRlciBvciBuYXJyb3dlci5cbkBtaXhpbiBtZWRpYS1icmVha3BvaW50LW9ubHkoJG5hbWUsICRicmVha3BvaW50czogJGdyaWQtYnJlYWtwb2ludHMpIHtcbiAgJG1pbjogYnJlYWtwb2ludC1taW4oJG5hbWUsICRicmVha3BvaW50cyk7XG4gICRtYXg6IGJyZWFrcG9pbnQtbWF4KCRuYW1lLCAkYnJlYWtwb2ludHMpO1xuXG4gIEBpZiAkbWluICE9IG51bGwgYW5kICRtYXggIT0gbnVsbCB7XG4gICAgQG1lZGlhIChtaW4td2lkdGg6ICRtaW4pIGFuZCAobWF4LXdpZHRoOiAkbWF4KSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH0gQGVsc2UgaWYgJG1heCA9PSBudWxsIHtcbiAgICBAaW5jbHVkZSBtZWRpYS1icmVha3BvaW50LXVwKCRuYW1lLCAkYnJlYWtwb2ludHMpIHtcbiAgICAgIEBjb250ZW50O1xuICAgIH1cbiAgfSBAZWxzZSBpZiAkbWluID09IG51bGwge1xuICAgIEBpbmNsdWRlIG1lZGlhLWJyZWFrcG9pbnQtZG93bigkbmFtZSwgJGJyZWFrcG9pbnRzKSB7XG4gICAgICBAY29udGVudDtcbiAgICB9XG4gIH1cbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/login-page/login-page.component.ts":
/*!****************************************************!*\
  !*** ./src/app/login-page/login-page.component.ts ***!
  \****************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app-service.service */ "./src/app/app-service.service.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../login.service */ "./src/app/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-spinner */ "./node_modules/ngx-spinner/fesm5/ngx-spinner.js");






var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(apiService, loginService, router, spinner) {
        this.apiService = apiService;
        this.loginService = loginService;
        this.router = router;
        this.spinner = spinner;
        this.disableLogin = false;
        this.isShow = true;
        this.validationStatus = true;
        this.model = { 'orgName': "", 'passwd': "", 'userName': "" };
        this.isActive = true;
        this.loginSatus = false;
    }
    LoginPageComponent.prototype.getUsers = function () {
        var _this = this;
        this.apiService.getUsers()
            .subscribe(function (users) { return _this.users = users; });
    };
    LoginPageComponent.prototype.ngOnInit = function () {
        this.spinner.hide();
    };
    LoginPageComponent.prototype.authenticateUser = function () {
        var currentUserName = this.model.userName;
        var currentPass = this.model.passwd;
        return this.loginService.login(currentUserName, currentPass);
    };
    LoginPageComponent.prototype.isInvalidUserNameOrPassword = function (username, password) {
        var reg = new RegExp('/[\^/\\.,]/g');
        if (reg.test(username) || reg.test(password)) {
            return true;
        }
        return false;
    };
    LoginPageComponent.prototype.toggleDisplay = function () {
        this.isShow = !this.isShow;
    };
    LoginPageComponent.prototype.toggleView = function () {
        this.isActive = !this.isActive;
    };
    LoginPageComponent.prototype.onSubmit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.spinner.show();
                if (this.isInvalidUserNameOrPassword(this.model.userName, this.model.passwd)) {
                    this.spinner.hide();
                    alert("Username or Password cannot contain '/', '\\', '.', '^' or  ','");
                }
                else {
                    response = this.authenticateUser().subscribe(function (response) {
                        if (response.success) {
                            localStorage.setItem('currentUser', _this.model.userName.toUpperCase());
                            _this.isShow = false;
                            _this.apiService.setCurrentUser(localStorage.getItem('currentUser'));
                            _this.router.navigate(['/dashboard', _this.model.userName.toUpperCase()]);
                        }
                        else {
                            _this.toggleDisplay();
                            localStorage.removeItem('currentUser');
                            _this.spinner.hide();
                        }
                    });
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPageComponent.ctorParameters = function () { return [
        { type: _app_service_service__WEBPACK_IMPORTED_MODULE_2__["AppService"] },
        { type: _login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"] }
    ]; };
    LoginPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-page',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login-page.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-page/login-page.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login-page.component.scss */ "./src/app/login-page/login-page.component.scss")).default]
        })
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./src/app/login.service.ts":
/*!**********************************!*\
  !*** ./src/app/login.service.ts ***!
  \**********************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.login = function (username, password) {
        var user = { "username": username, "password": password };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]()
            .set('Content-Type', 'application/json');
        return this.http.post("/api/users/login", user, {
            headers: headers
        });
    };
    LoginService.prototype.logout = function () {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    };
    LoginService.prototype.isAuthenticated = function () {
        return this.http.get("/api/users/isAuthenticated");
    };
    LoginService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
    ]; };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Pawan\webapp\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map