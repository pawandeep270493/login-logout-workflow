const express = require("express");
var cookieParser = require('cookie-parser'); // module for parsing cookies

const app = express();


app.use(cookieParser());

app.use(express.static(__dirname + '/public'));

// Init Middleware
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("API ENGAGED!"));

// Define Routes
app.use("/api/users", require("./routes/api/users"));

app.get('*', function(req, res){
    res.sendFile(__dirname + '/public/index.html');
});
  

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => console.log(`Server started on port: ${PORT}`));
