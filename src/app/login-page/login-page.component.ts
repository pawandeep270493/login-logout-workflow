import { Component, OnInit } from '@angular/core';
import { AppService } from '../app-service.service';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnInit {

  constructor(private apiService: AppService, private loginService: LoginService,
    private router: Router,
    private spinner: NgxSpinnerService) { }

  disableLogin = false;
  isShow = true;
  users: any;
  validationStatus = true;
  model = { 'orgName': "", 'passwd': "", 'userName': "" }
  isActive = true;
  loginSatus = false;

  getUsers(): void {
    this.apiService.getUsers()
      .subscribe(users => this.users = users);
  }

  ngOnInit() {
    this.spinner.hide();
  }

  authenticateUser() {
    var currentUserName = this.model.userName;
    var currentPass = this.model.passwd;
    return this.loginService.login(currentUserName, currentPass);
  }



  isInvalidUserNameOrPassword(username: string, password: string) {
    let reg = new RegExp('/[\^/\\.,]/g');
    if (reg.test(username) || reg.test(password)) {
      return true;
    }

    return false;
  }


  toggleDisplay() {

    this.isShow = !this.isShow;
  }

  toggleView() {
    this.isActive = !this.isActive;
  }



  async onSubmit() {
    this.spinner.show();
    if (this.isInvalidUserNameOrPassword(this.model.userName, this.model.passwd)) {
      this.spinner.hide();
      alert("Username or Password cannot contain '/', '\\', '.', '^' or  ','");
    }
    else {
      //let response = {"success":"false"};
      // let response:any = await this.validateUser().toPromise();
      // if (!isNullOrUndefined(response)) {
      //   if (!response.success) {
      //     this.toggleDisplay();
      //   } else {
      //     this.isShow = false;
      //     this.dlsService.setCurrentUser(this.model.userName);
      //     this.router.navigate(['/dashboard']);
      //   }
      // }

      //Dummy Authentication

      let response: any = this.authenticateUser().subscribe((response) => {


        if (response.success) {
          localStorage.setItem('currentUser', this.model.userName.toUpperCase())
          this.isShow = false;
          this.apiService.setCurrentUser(localStorage.getItem('currentUser'));
          this.router.navigate(['/dashboard', this.model.userName.toUpperCase()]);

        } else {

          this.toggleDisplay();
          localStorage.removeItem('currentUser');
          this.spinner.hide();
        }
      });
    }
  }
}
