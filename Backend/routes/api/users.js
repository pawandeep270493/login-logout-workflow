const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { check, validationResult, body } = require("express-validator");
const config = require("config");
const credentials = require("../../data/credentials.json");
const crypto = require('crypto');


const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));


router.post(
  "/login",
  [
    check("username", "username is required")
      .not()
      .isEmpty(),
    check(
      "password",
      "Password is required"
    ).not()
      .isEmpty(),
    body(["username", "password"]).custom(value => {
      let reg = /[/\\,.^]/;
      if (reg.test(value)) {
        throw new Error("username and/or password has special characters");
      }
      return true;
    }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const { username, password } = req.body;
    try {
      // Check if User is Valid
      let isValid = isValidUser(username, password);
      if (!isValid) {
        return res
          .status(400)
          .json({ success: false, errorMessage: 'Login Error' });
      }


      let hash = computeHash(username);


      let cookieJson = `${username}-${hash}`;

      await sleep(Math.random() * 3000); //Sleeping for less than 3 seconds

      res.cookie('data', (cookieJson));

      res.status(200).json({ success: true, errorMessage: '' });


    } catch (err) {
      console.log(err.message);
      res.status(500).send({ success: false, errorMessage: err.message });
    }
  }
);


router.get('/isAuthenticated', (req, res) => {
  let cookie = req.cookies.data;

  if (cookie != undefined) {

    let cookieValue = cookie.split('-');

    let hash = computeHash(cookieValue[0]);

    if (hash == cookieValue[1]) {
      res.status(200).json({ isAuthenticated: true});

    }
    else {
      res.send(401).json({ isAuthenticated: false});
    }
  }
  else {
    res.send(401).json({ isAuthenticated: true});
  }

})

const isValidUser = async (username, password) => {



  var registeredPassowrd = credentials[username];
  if (registeredPassowrd == (password)) {
    return true;
  }
  return false;

}

const computeHash = (data) => {

  let key = config.get('secret_key');

  let hash = crypto.createHash('md5').update(data + key).digest("hex");

  return hash

}

module.exports = router;
