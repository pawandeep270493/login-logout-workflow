# Sample App : Pawandeep Singh
This repository contains the source code of the sample app created for Ion Trading. It has both the front-end and back-end components of the app. Following is the list of technologies used in this app

* Angular8 - Frontend framework
* Bootstrap4 - Frontend Style theme
* Express - Backend framework
* NPM - Dependency management

## Setting up development environment
### Pre-requisites
1. Install [GIT](https://git-scm.com/downloads)
2. Install [NodeJS](https://nodejs.org/en/download/) version 12.16.0

### Setting up local server
1. Clone GitHub repository using following command
```
git clone https://pawandeep270493@bitbucket.org/pawandeep270493/login-logout-workflow.git
```
    
2. Go to the root folder of above cloned repository

3. Install the required dependencies using following command
```
npm i
```

4. Start the local web server using following command
```
npm run start:dev
```

5. Open following link in your browser to view application
```
http://localhost:3001
```