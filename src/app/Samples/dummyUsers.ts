export const users: any[] = [
  {
    username: 'pawan',
    password: 'p1234',
    data: {
      books: [
        {
          bookName: "Javascript Applications",
          bookEdition: "Second Edition",
          bookThumbnail: "assets\\B1.jpg"
        },
        {
          bookName: "Eloquent Javascript",
          bookEdition: "Second Edition",
          bookThumbnail: "assets\\B2.jpg"
        }
      ]
    }
  },
  {
    username: 'test',
    password: 't1234',
    data: {
      books: [
        {
          bookName: "Javascript Applications",
          bookEdition: "Second Edition",
          bookThumbnail: "assets\\B1.jpg"
        },
        {
          bookName: "Eloquent Javascript",
          bookEdition: "Second Edition",
          bookThumbnail: "assets\\B2.jpg"
        }
      ]
    }
  }
];